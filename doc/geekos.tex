\documentclass[letterpaper]{article}

\title{GeekOS for the Raspberry Pi - Overview}
\date{Last Modified \today}
\author{Eric Harrison}

\usepackage{fullpage}

\usepackage{calc}
\usepackage[colorlinks=true,urlcolor=blue,letterpaper,bookmarks]{hyperref}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{tabularx}

\graphicspath{ {./images/} } 

\begin{document}

\maketitle
\bigskip

\begin{center}
\textbf{Abstract}\\
\end{center}
This document gives an overview of the GeekOS distribution for the Raspberry Pi and related background on QEMU and 
ARM. GeekOS for the Raspberry Pi is an adaptation of GeekOS \\(\htmladdnormallink{https://www.cs.umd.edu/~hollings/cs412/s14/GeekOSoverview.pdf}{https://www.cs.umd.edu/~hollings/cs412/s14/GeekOSoverview.pdf}).
	It describes some operations in GeekOS in more detail,
	 in particular, initialization, low-level interrupt handling and 
	 context switching, thread creation, and user program spawning. All references to ``GeekOS" in this document shall refer to GeekOS for the Raspberry Pi and ARM unless stated otherwise.  

\section{Introduction}

The source repository for GeekOS contains only source code (in C, ARM assembly, Makefile, Perl). After executing the makefiles, it will also contain object code and executables that can run on a PC-like hardware platform (x86 processor, memory, IO devices, etc). In this case, the hardware platform is simulated by QEMU. Working on an x86 platform it will be necessary to use a cross-compiler toolchain (that builds for ARM BCM2708).\\
		Unfortunately, QEMU does not have native Raspberry Pi hardware support. To resolve this discrepancy, several boot options have been added to the Makefile.
		\\
		The directories of GeekOS are organized as follows:

		\begin{itemize}
		\item Directory 
		\texttt{build}
		has makefiles for starting QEMU with GeekOS and user programs. Its 
		subdirectories, which are initially
		empty, will hold object and executable modules. In particular, 
		there will be an elf file, \texttt{kernel.elf}, and an image file, \texttt{kernel.img}, representing the compiled OS. Currently, user programs are part of the kernel image as part of an in-memory filesystem.
		\item Directories 
		\texttt{src/geekos}
		and 
		\texttt{include/geekos}
		contains the kernel code. Executed by 
		QEMU’s processor in kernel mode.
		\item Directory 
		\texttt{src/user}
		contains user programs that 
		run on GeekOS. Executed by QEMU’s 
		processor in user mode.  
		\item Directory 
		\texttt{src/libc}
		contains C entry functions for system calls. User programs call these 
		functions to obtain OS services. Executed by QEMU’s processor in user mode (but switches to 
				kernel mode while executing system cal
				ls). Header files are in directory 
		\texttt{include/libc}.
		\item Directory 
		\texttt{src/common}
		has heap manager 
		\texttt{bget}
		, output formatter 
		\texttt{fmtout}
		, string manipulation 
		\texttt{string}
		, and 
		\texttt{memmove}
		. Nothing specific to operating systems 
		here. Header files are in directory 
		\texttt{include/libc}
		.
		\item Directory 
		\texttt{src/tools}
		contains code for constructing the disk images that is supplied to QEMU. 
		In particular, 
		\texttt{buildFat.c}
		constructs the PFAT file system on diskc.
		\item Directory 
		\texttt{scripts}
		contains Perl scripts, some of 
		which are used in the makefiles
		\end{itemize}

\section{QEMU}

QEMU simulates a PC-like hardware. The QEMU configuration achieved by makefile includes the 
following. Below addresses are referred to by their hex values or their source code names or both, for 
example, ``\texttt{0xB800}'' or ``\texttt{VIDMEM\_ADDR}'' or ``\texttt{0xB800} / \texttt{VIDMEM\_ADDR}''.

\begin{itemize}
\item Processor: ARM1176 JZF-S
\item BIOS: When QEMU is started 
(corresponding to power up), BIOS loads the ELF segments in kernel.elf into memory, and the processor
	then starts executing at address \texttt{0x8000}.  
\item Memory: 512 MBytes.  
\item VIC (Vectored Interrupt Controller): 
	 receives interrupts from IO devices (keyboard, 
			 dma, ide, floppy drive) and funnels them to the processor. There are no ports as in x86; rather, all peripherals are memory-mapped.
\item GPIO (General-purpose input/output): A set of configurable input/output pins. Of most importance are pins 14 and 15, which can be configured to be used as serial input/output.
\item UART (Universal asynchronous receiver/transmitter): Provides serial interface to write and read from console.
\end{itemize}

\section{ARM Architecture}

The ARM processor can be in one of several modes. Three of them are relevant to GeekOS: ``IRQ", ``SVC", and ``User" modes. IRQ mode is entered due to a hardware interrupt (for GeekOS this will be the system timer or keyboard). SVC, or supervisor, is the privileged mode used inside the kernel. User mode is the default privilege level for user programs. All assembly instructions in ARM are 4 bytes long, except for special Thumb Mode instructions (unused) which are 2 bytes. While most registers are shared across all modes, certain registers are ``banked" across modes, which means that there are separate copies of the register which are completely independent depending on which mode the processor is in.\\\\
There is no segmentation in ARM. Processors use a 2 level page table which by default uses 16kB first level pages. Each entry of the first level page table will either point to a 1MB region of memory or to a second level page table of size 1kB. Each entry in the second level page table covers 4kB.\\\\
Interrupts in ARM are one of 6 types: undefined instruction, software interrupt, prefetch abort, data abort, IRQ, and FIQ. FIQ is similar to an IRQ hardware interrupt except that more registers are banked, eliminating the need to save extra registers. GeekOS does not use FIQ. There are no interrupt gates in ARM. Instead, there is a single interrupt vector table. The interrupt vector table is a table of 7 4-byte instructions. Upon receiving an interrupt, the processor will change mode depending on the interrupt received (SVC for a software interrupt, IRQ for others) and then execute the instruction stored in the interrupt table. Each interrupt type has its own index into the interrupt table. The instructions in the interrupt table are branches to the start of an interrupt handler.\\\\
Before executing and after returning from an interrupt handler, non-banked registers must be stored into the process control block. A summary of ARM registers is below:
\\
\begin{itemize}
\item \texttt{r0} : General-purpose. Similar to \texttt{eax} in x86 format. Acts as argument 1 to functions and as the return value from functions. Caller-save.
\item \texttt{r1-r3} : General-purpose. Arguments 2-4 to functions. Caller-save.
\item \texttt{r4-r10} : General-purpose. Callee-save.
\item \texttt{r11} : Frame pointer.
\item \texttt{r12} : General-purpose. Callee-save.
\item \texttt{r13} : AKA \texttt{sp}. Stack pointer.
\item \texttt{r14} : AKA \texttt{lr}. Link register. Represents return address from a function. Caller-save.
\item \texttt{r15} : AKA \texttt{ip}. Instruction pointer.
\item \texttt{cpsr} : Current program status register. Used to change modes between SVC, IRQ, and User mode. Also contains condition flags that are set after conditional instructions, and has a flag to disable or enable IRQ interrupts.
\item \texttt{spsr} : Saved program status register. Stores the current program status register during interrupt handling.
\end{itemize}

In addition to these registers, ARM has special system control co-processor registers. For GeekOS the only relevant co-processor is CP15, which controls the memory management unit to enable paging and set the base address of the level 1 page table.
\\\\
There are no IO ports on ARM to access the IO Bus. All interfacing with the IO bus is done with memory mapped registers. A guide to the Raspberry Pi peripherals is at https://www.raspberrypi.org/wp-content/uploads/2012/02/BCM2835-ARM-Peripherals.pdf. A summary of the base register addresses for QEMU emulation peripherals is given below, taken from the QEMU source code.
\\
\begin{center}
\begin{tabular}{| l | c | }
\hline
0x10000000 & System registers \\ \hline
0x10001000 & PCI controller config registers. \\ \hline
0x10002000 & Serial bus interface. \\ \hline
0x10003000 & Secondary interrupt controller. \\ \hline
0x10004000 & AACI (audio).  \\ \hline
0x10005000 & MMCI0. \\ \hline
0x10006000 & KMI0 (keyboard). \\ \hline
0x10007000 & KMI1 (mouse).  \\ \hline
0x10008000 & Character LCD Interface.  \\ \hline
0x10009000 & UART3.  \\ \hline
0x1000a000 & Smart card 1.  \\ \hline
0x1000b000 & MMCI1.  \\ \hline
0x10010000 & Ethernet.  \\ \hline
0x10020000 & USB.  \\ \hline
0x10100000 & SSMC.  \\ \hline
0x10110000 & MPMC.  \\ \hline
0x10120000 & CLCD Controller.  \\ \hline
0x10130000 & DMA Controller.  \\ \hline
0x10140000 & Vectored interrupt controller.  \\ \hline
0x101d0000 & AHB Monitor Interface.  \\ \hline
0x101e0000 & System Controller.  \\ \hline
0x101e1000 & Watchdog Interface.  \\ \hline
0x101e2000 & Timer 0/1.  \\ \hline
0x101e3000 & Timer 2/3.  \\ \hline
0x101e4000 & GPIO port 0.  \\ \hline
0x101e5000 & GPIO port 1.  \\ \hline
0x101e6000 & GPIO port 2.  \\ \hline
0x101e7000 & GPIO port 3.  \\ \hline
0x101e8000 & RTC.  \\ \hline
0x101f0000 & Smart card 0.  \\ \hline
0x101f1000 & UART0.  \\ \hline
0x101f2000 & UART1.  \\ \hline
0x101f3000 & UART2.  \\ \hline
0x101f4000 & SSPI.  \\ \hline
\end{tabular}
\end{center}

\section{Booting Up}

The file \texttt{Makefile.common} has a variable \texttt{BOOT\_OPTS} that can control whether building for QEMU or the Raspberry Pi. In order to build for QEMU, only the flag \texttt{-DSERIAL\_OUTPUT} should be present. In order to build for the Raspberry Pi, both \texttt{-DSERIAL\_OUTPUT} and \texttt{-DRPI\_BOOT} should be present. In order to add support for VGA output, the optional flag \texttt{-DVGA\_OUTPUT} can also be added to \texttt{BOOT\_OPTS}.\\

Running as QEMU can be done simply via \texttt{make run}. In order to run the raspberry pi, type \texttt{make}, which will produce an image file, \texttt{kernel.img}. Copy this file to the Raspberry Pi SD Card in order to use it as the kernel image.\\

GeekOS runs via a serial interface and optional VGA output. The Adafruit 954 serial cable can be used to connect the Raspberry Pi to a USB port on a computer. A serial console connection is required. On Windows, PuTTy can be used to connect to a COMM port. The serial connection should look as follows: \\
\begin{center}
\includegraphics[scale=0.3]{adafruitserial}
\\
Color Coding: Red (Power), Black (Ground), White (Transmit),  Green (Receive)
\end{center}

Additionally, an HDMI cable can be used to connect the Raspberry Pi output to a monitor. In order to do this, make sure the \texttt{-DVGA\_OUTPUT} boot option is specified when making \texttt{kernel.img} add the following lines to \texttt{config.txt} in the SD Card:

\begin{lstlisting}
hdmi_group=1
hdmi_mode=1
\end{lstlisting}

\begin{center}
\includegraphics[scale=0.1]{vgaoutput}\\
Sample image of VGA output from the Raspberry Pi.
\end{center}

After the kernel starts executing, it will branch to \texttt{Main} and perform the following:

\begin{itemize}
\item \texttt{Init\_UART} (defined in \texttt{geekos/uart.c}) : Enables the UART for serial I/O, and installs an interrupt handler for serial input.
\item \texttt{Init\_VGA} (defined in \texttt{geekos/vga.c}) : (Non-QEMU only) Acquires a framebuffer from the BIOS where VGA output can be written.
\item \texttt{Init\_Scheduler} (defined in \texttt{geekos/kthread.c}) : Sets up the main kernel thread object and stack, and sets up the current thread.
\item \texttt{Init\_Interrupts} (defined in \texttt{geekos/int.c}) : Copies the interrupt vector table defined in \texttt{lowlevel.S} to address 0, the address of the IVT. Enables interrupts.
\item \texttt{Init\_Mem} (defined in \texttt{geekos/mem.c}):
	Treats memory as a sequence of 4KB pages. Creates (in kernel memory) a list of 
			Page
			 structs 
			corresponding to the memory pages, each storing the 
			attributes of its page (kernel, available for 
					users, allocated, etc). Global variable 
			\texttt{g\_pageList}
			 points to the list. Also creates a list of the 
			available pages (
					\texttt{s\_freeList}
					).  
			Calls 
			\texttt{Init\_Heap}
			 (defined in 
				geekos/malloc.c
			 ) to initialize the kernel 
			heap. (Malloc itself is 
					implemented by 
					bget
					.)  
\item \texttt{Init\_Startup\_Threads} (defined in \texttt{geekos/kthread.c}) : Starts the idle and reaper threads.
\item \texttt{Init\_Keyboard} (defined in \texttt{geekos/keyboard.c}) : Sets the keyboard buffer to be initially empty.
\item \texttt{Init\_Timer} (defined in \texttt{geekos/timer.c}) : On QEMU, loads the one-shot timer with the initial value, enable one-shot timer, and enables the timer module. On the Raspberry Pi, loads the initial counter with the standard number of cycles. Both QEMU and Raspberry Pi then install an interrupt handler for the timer.
\item \texttt{Init\_VM} (define in \texttt{geekos/paging.c})
\item \texttt{Init\_CRC32} (defined in \texttt{geekos/crc32.c}) : Initializes the CRC Table
\item \texttt{Init\_Traps} (defined in \texttt{geekos/trap.c}) : Installs an SVC handler to catch software interrupts and an undefined handler to catch protection faults.
\item \texttt{Init\_Paging} (define in \texttt{geekos/paging.c})
\item \texttt{Init\_PFAT} (defined in \texttt{geekos/pfat.c}) : Registers the PFAT file system interface to the 
virtual file system.
\item \texttt{Init\_GFS2}, \texttt{Init\_GOSFS}, \texttt{Init\_CFS} : Start all of the different filesystem types.
\item \texttt{Init\_Pseudo\_Block\_Device} : Starts the pseudo-block device request thread.
\item \texttt{Mount\_Root\_Filesystem} : mounts the root drive (diskc) as a PFAT file system to the virtual file system (in vfs.c) at root prefix “/”. 
\item \texttt{Spawn\_Init\_Process} : starts the user shell program.  
\end{itemize}

\section{Context Switching}

\subsection{Context state}

The context state of a thread is stored in three structures, all reachable from the first:  
\begin{itemize}
\item A \texttt{Kernel\_Thread} struct (defined in \texttt{geekos/kthread.h}). This contains the kernel stack 
 pointer, various kernel-related state (refcount, pid, 
		 etc.), and pointers to st
 ack and user context (see 
		 below).
\item A stack page. This is the kernel stack of the thread. When the thread is not executing, the 
 processor
  state of the thread is stored here as follows:\\
	\texttt{r15} (\texttt{pc}) \\
	\texttt{r14} (\texttt{lr}) \\
	\texttt{r13} (\texttt{sp}) \\
	\texttt{r12} - \texttt{r0} \\
	\texttt{spsr} \\
	Thus, the thread can be resumed simply by popping registers \texttt{r0} to \texttt{r15} and setting the CPSR back to the SPSR.
\item A \texttt{User\_Context}
	 struct (defined in 
			 \texttt{geekos/user.h}
			 ). This is present only if the thread is a 
	 user thread, i.e., started by spawing a user program. It contains user-level OS state (entry address, argument block, initial stack, etc.).
\end{itemize}

\subsection{Stopping and resuming threads}

	 The context switching code appears in the interrupt vector table and the 
	 \texttt{Switch\_To\_Thread} function (both in file \texttt{lowlevel.S}):  

\begin{itemize}
\item \texttt{Interrupt\_Vector\_Table} : The processor sets the instruction pointer to \texttt{0x0000 + exceptionNumber*4} when it receives an interrupt. Each exception has different 4-byte instruction that in the interrupt table. The processor will jump to the relevant function from the interrupt vector table after receiving an interrupt. In all cases, the function will construct the interrupt state of the current thread, call the C interrupt handler, and finally either
resume the current thread or switches it out and switches in a thread from the run queue.
\item \texttt{Switch\_To\_Thread} :	Assumes the following (verify each
					 and check if it matters):  
	\begin{itemize}
	\item  the current thread got here via a call (not an 
						interrupt) with a thread 
				pointer arg on stack;  
	\item  the current thread has already been moved to the run/wait queue;  
	\item if the current thread has a user context 
				then it is exiting (Is 
						this important? ).  
	\item	Constructs the context of the current thread 
				and switches in the thre
				ad pointer’s threaad.  
	\end{itemize}
\end{itemize}

In both cases, the context switching 
				code makes use of the kernel stack of the current thread (i.e., the 
						one to be switched out).

\subsection{\texttt{Handle\_Exception}}

The assembly macro \texttt{Handle\_Exception} is used during interrupt handling to set up the stack before the C interrupt handler is called. It checks to see whether the interrupt occurred in user or supervisor mode.

\begin{lstlisting}
.macro Handle_Exception
// Store banked link register on IRQ stack, and use it to check mode
ldr sp, =IRQ_STACK
stmdb sp!, {lr}
mrs lr, spsr
and lr, #0x1f
mov sp, #0x10
teq sp, lr

// Restore LR
ldr sp, =IRQ_STACK
ldmdb sp, {lr}

beq 1f
	// If previous mode was not user mode, use Handle_Exception
	Handle_Nonuser_Exception
b 2f
1:
	// If previous mode was user mode, use Handle_User_Excpetion
	Handle_User_Exception
2:
.endm
\end{lstlisting}

Due to the nature of banked registers, the instructions for handling exceptions that occurred in user mode as opposed to supervisor mode are different.

\begin{lstlisting}
.macro Handle_User_Exception
ldr sp, =g_currentThreads       // Load kernel stack pointer for current thread
ldr sp, [sp, #0]                // Load address of current thread
ldr sp, [sp, #0]                // Load address of current thread stack pointer

_Save_User_Registers

ldr lr, =Restore_User_Mode

mov r0, sp                      // Pass interrupt state as first argument
.endm
\end{lstlisting}

\begin{lstlisting}
.macro Handle_Nonuser_Exception
// Need to store link register and spsr somewhere - use small IRQ Stack
ldr sp, =IRQ_STACK
stmdb sp!, {lr}
mrs lr, spsr
stmdb sp, {lr}

// Swap to SVC mode to access PCB stack
mrs lr, cpsr_all
bic lr, lr, #0x1f
orr lr, lr, #0x13
msr cpsr_all, lr

// Come back to store link register later.
stmdb sp, {lr}
mov lr, sp
sub lr, #8
stmfd lr, {r0-r13}
ldmdb sp, {lr}

// Acquire pointer to IRQ Stack and get saved lr/spsr
ldr r0, =IRQ_STACK
ldmdb r0, {r1, r2}

// Push lr and banked lr onto stack at bottom
stmfd sp!, {r2}
stmfd sp!, {lr}

// Push saved spsr onto stack (it goes at the top)
sub sp, #56
stmfd sp!, {r1}

mov r0, sp
ldr lr, =Restore_SVC_Mode
.endm
\end{lstlisting}

\subsection{\texttt{Switch\_To\_Thread(threadptr)}}

\begin{lstlisting}
Switch_To_Thread:

# Store current interrupt state on stack
_Save_Registers

# Set esp in kthread struct
ldr sp, =g_currentThreads       // Load kernel stack pointer for current thread
ldr sp, [sp, #0]                // Load address of current thread
str lr, [sp, #0]                // Store address of current thread stack pointer

# Make new thread current
ldr sp, =g_currentThreads
str r0, [sp, #0]

# Swap to new thread's stack
ldr sp, [r0]

# Activate the user context, if necessary.
ldr lr, =End_Switch_Thread
ldr pc, =Switch_To_User_Context

\end{lstlisting}

\section{Starting threads and spawning user programs}

\subsection{Starting a kernel thread}

\texttt{Start\_Kernel\_Thread(startFunc, arg, priority)}
\begin{itemize}
\item \texttt{Create\_Thread} :
	\begin{itemize}
		\item get memory for kthread struct and for stack;  
		\item initialize kthread fields: stackPage, esp, numTicks, pid, etc.  
	\end{itemize}
\item \texttt{Setup\_Kernel\_Thread} :
	\begin{itemize}
		\item configure kthread’s stack so that when this kthread is switched in (in lowlevel.S
			), 
	it executes 
	\texttt{Launch\_Thread}
	, then 
	\texttt{startFunc(arg)}
	, then 
	\texttt{Shutdown\_Thread}
	. \\
	Stack bottom:\\
	\texttt{pc = startFunc, lr = Shutdown\_Thread, sp = stackPage, ..., r0 = arg, \\spsr = KERNEL\_CPSR}\\
	Stack top
		\end{itemize}
\item Add to runQ 
\end{itemize}

\subsection{Starting a user thread}
\texttt{Start\_User\_Thread(userContext)}

\begin{itemize}
\item \texttt{Create\_Thread} :  
	\begin{itemize}
	\item get memory for kthreadd object and stack
	; initialize (as with
			 kernel thread) 
	\end{itemize}

\item \texttt{Setup\_User\_Thread} :
	\begin{itemize}
	\item point 
	\texttt{kthread.userContext}
	 to 
	 \texttt{userContext}
	\item fix up (kernel) stack, similar to above kernel threads: \\
		Stack bottom:\\
		\texttt{pc = entryAddr, lr = 0, sp = stackPointerAddr, ..., r1 = argBlockAddr, r0 = numArgs, spsr = USER\_CPSR}\\
		Stack top
	\end{itemize}

\item Add to runQ
\end{itemize}

\subsection{Spawning a user program}

\texttt{Spawn(programPathname, command, userContext)}
\begin{itemize}
	\item Load user prog:
	\begin{itemize}
		\item get file from file system (
			\texttt{vfs.c}
			, 
			\texttt{pfat.c}
			), 
	unpack into elf header and content, extract \texttt{exeFormat} (
			\texttt{elf.c}:
			). 
		\item get max virtual address of program
	 and \texttt{argBlockSize} (from \texttt{exeFormat}), 
	 acquire memory 1 for program segment, arg block and user stack, 
	 load program segment into memory 1, 
	 format argblock in memory 1, 
	 acquire memory 2 for usercontext and initialize fields (size, ldt, entry point).  
	\end{itemize}

\item \texttt{Start\_User\_Thread(userContext)}

\end{itemize}

\section{Filesystem Overview}

The same filesystem is used on GeekOS for ARM as is it for GeekOS on x86.

\section {Block Device Driver}

Currently, the main disk is loaded into memory as part of the kernel. Block device requests get sent to a pseudo-block device driver which reads and writes to an in-memory filesystem. It has a queue of threads with requests (\texttt{s\_fakeBlockWaitQueue}) and a queue of which blocks the threads are requesting (\texttt{s\_fakeBlockRequestQueue}).\\\\
		Reading and writing a block is done by reading and writing the relevant regions of memory, because the block device driver is actually an interface for an in-memory filesystem. The linker symbols \texttt{\_\_pfat\_start} and \texttt{\_\_pfat\_end} define the start and end regions of the in-memory disk.

\section {OS Subsystems}

Each subsection below identifies a “subsystem” of
 the OS and lists the associated files.  

\subsection{Utilities}

The following files provide non-OS-specific functionality, such as debug macros, output formatting, 
 strings, generic lists, linking maps, etc.  

\begin{itemize} 
\item \texttt{libc/bget.h}
\item \texttt{common/bget.c}
\item \texttt{geekos/bget.h} : heap structure.  
\item \texttt{malloc} : memory manager; wrapper for bget.
\item \texttt{geekos/bitset.h—c} : bitset structure.  
\item \texttt{libc/fmtout.h}, \texttt{common/fmtout.c}, \texttt{geekos/fmtout.h} : output formatting.  
\item \texttt{geekos/ktypes.h} : aliases to integer and char  types, min/max functions, etc.
\item \texttt{geekos/kassert.h} : debugging macros (KASSERT, TODO, PAUSE, etc). 
\item \texttt{common/libuser.h} : includes user libray (\texttt{conio.h}, \texttt{sema.h}, \texttt{sched.h}, \texttt{fileio.h})
\item \texttt{geekos/list.h} : generic list structure.  
\item \texttt{common/memmove.h} : standard “memory move” function.  
\item \texttt{geekos/range.h} : geekos/range.h
\item \texttt{libc/string.h} , \texttt{common/string.c}, \texttt{geekos/string.h} : string manipulation.  
\item \texttt{geekos/symbol.h—asm} : symbol mangling macros (for linking C and asm).  
\end{itemize}

\subsection{Memory system}

Physical memory managment: divides physical memory into 4KB pages, keeps track of the pages 
 (kernel, user, free, kernel heap, etc.), gives out 
 memory when needed (e.g., 
		 for process creation, data 
		 structures, etc.), gets back memory when released. \\
 Files: \texttt{geekos/malloc.*}, \texttt{geekos/mem.*}.  

\subsection{Process management}

 Kernel process managment: kernel thread state; thread queues; creation, deletion and switching of kernel threads; thread signalling and synchronization.\\
 Files: \texttt{geekos/kthread.*}, \texttt{geekos/lowlevel.S}  (function \texttt{Switch\_To\_Thread}). \\
 User process management: augmenting kernel threads with user context and user process creation, deletion, switching. \texttt{libc/process.*}, \texttt{geekos/user.*}
	, 
	\texttt{geekos/lowlevel.S}
	 (function 
		\texttt{Switch\_To\_Thread}
	 ). \\
	 User program loading: loading a user executable (obtained from diskc) into memory. \\
	 Files: 
	 \texttt{geekos/elf.*}
	 , 
	 \texttt{geekos/argblock.*}

\subsection{Interrupt system}

This comprises the mapping from interrupt entry points (in IDT) to interrupt handlers and the mapping from interrupt handlers back to resuming the interrupted processes. Covers both external (hardware) interrupts and internal interrupts (exceptions, traps).
\\ Files in \texttt{geekos}:
	\texttt{idt}
	, 
	\texttt{int}
	, 
	\texttt{irq}
	, 
	\texttt{trap}
	, 
	\texttt{lowlevel.S} \\
	 (tables \texttt{Local\_Interrupt\_Vector\_Table} and \texttt{Interrupt\_Vector\_Table}
	 ).  

\subsection{Syscall system}

	 Syscalls are all instances of an \texttt{svc 0x20} (the ARM equivalent of \texttt{int 0x80}); i.e., trap.c forwards it to the appr	opriate syscall handler. \\
		Files in \texttt{geekos} : \texttt{trap}
		 (function 
			\texttt{Syscall\_Handler}
		 ), 
		 \texttt{syscall}
		 .  

\subsection{Device drivers}

		 This comprises the functions for I/O on hardware 
		 devices and the interrupt handlers for handling 
		 interrupts issued by 
		 these devices. 
		 \\
		Files in \texttt{geekos} : 
		 \texttt{timer}
		 , 
		 \texttt{screen}
		 , 
		 \texttt{keyboard}
		 , 
		 \texttt{vga},
		 \texttt{uart},
		 \texttt{mailbox},
		 \texttt{pseudoblock}
		 . \\
		Files in \texttt{common} :
		\texttt{gpio}, \texttt{mmio}.

\subsection{Console}

The console is the user-level “device”
consisting of keyboard and screen.  
			Files: 
			\texttt{include/libc/conio.h}
			, 
			\texttt{src/libc/conio.c}
			, 
			\texttt{geekos/syscall}
			 (handlers for 
				syscalls in 
				\texttt{conio}
			 ).  

\subsection{File system}
			 This comprises the virtual file system, the user interface to the virtual file system, the concrete file 
			 systems (pfat, gsfs2, gosfs) that can be mounted on the 
			 virtual file system, and the block device interface 
			 to the hardware disk devices. \\ 
			 OS side (all in 
					 \texttt{geekos}
					 ): 
				 \texttt{vfs}
				 , 
				 \texttt{pfat}
				 , 
				 \texttt{gosfs}
				 , 
				 \texttt{gsfs2}
				 , 
				 \texttt{blockdev}
				 , 
				 \texttt{bufcache}
				 , 
				 \texttt{syscall}
				  (
					 \texttt{fileio}
					 syscall handlers).  
					\\
				User side: 
					\texttt{libc/fileio.*}
					.  

\section{Memory organization after setup and after Main}

\begin{tabularx}{\textwidth}{| l | c | X | c | }
\hline
Address & Name(s) in source code & At end of \texttt{setup} & At end of \texttt{Main} \\ \hline
0x0000 & & start BIOS code/data & Interrupt Vector Table \\ \hline
0x5000 & \texttt{KERNEL\_THREAD\_OBJ} & & Main kernel thread page \\ \hline
0x6000 & \texttt{KERNEL\_STACK} & & Main kernel stack page \\ \hline
0x7000 & \texttt{KERNEL\_IRQ\_STACK} & Temporary stack used during IRQ & \\ \hline
0x8000 & \texttt{KERNEL\_START\_ADDR} & Start kernel image & \\ \hline
\texttt{\_\_end} & & End kernel image & Kernel heap \\ \hline
\texttt{\_\_end + KERNEL\_HEAP\_SIZE} & & & Kernel pagelist \\ \hline
& \texttt{pageListEnd} & & start available pages \\ \hline
& \texttt{endOfMem} & & end available pages \\ \hline
\end{tabularx}

\end{document}
