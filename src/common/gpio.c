/*
 * Interface for writing and reading bits from GPIO pins.
 */

#include <geekos/defs.h>
#include <geekos/gpio.h>
#include <geekos/mmio.h>
#include <geekos/timer.h>

void gpio_sync() {
 	__asm__ __volatile__ ("mcr p15, 0, %0, c7, c10, 4" : : "r" (0) : "memory");
	__asm__ __volatile__ ("mcr p15, 0, %0, c7,  c5, 4" : : "r" (0) : "memory");
}

void gpio_write(uint_t pin, unsigned char bit) {
	if(bit) { /* 1 */
		if(pin < 32) {
			mmio_writebit(REG_GPIO_SET0, pin);
		} else if(pin < NUM_GPIO_PINS) {
			mmio_writebit(REG_GPIO_SET1, pin % 32);
		}
	} else { /* 0 */
		if(pin < 32) {
			mmio_writebit(REG_GPIO_CLEAR0, pin);
		} else if(pin < NUM_GPIO_PINS) {
			mmio_writebit(REG_GPIO_CLEAR1, pin % 32);
		}
	}

	//gpio_sync();
	//Micro_Delay(1000);
}

unsigned char gpio_read(uint_t pin) {
	if(pin < 32) {
		return mmio_readbit(REG_GPIO_LEV0, pin);
	} else if(pin < NUM_GPIO_PINS) {
		return mmio_readbit(REG_GPIO_LEV1, pin % 32);
	} else { /* error */
		return -1;
	}
}

/*
 * The function select registers are arranged such that
 * the first 10 pins can be set by groups of 3 bits in the
 * first register, the 10 next by groups of 3 bits in the
 * second register, and so on.
 */
void gpio_function_select(uint_t pin, unsigned char function) {
	unsigned char i;
	for(i = 0; i < 3; i++) {
		if(function & (1 << i)) {
			mmio_setbit(REG_GPIO_SEL(pin), (pin % 10) * 3 + i);
		} else {
			mmio_unsetbit(REG_GPIO_SEL(pin), (pin % 10) * 3 + i);
		}
	}
}
