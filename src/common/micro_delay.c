/*
 * Micro delay timer for use in kernel or user programs (if timer register access enabled).
 */

#include <geekos/timer.h>
#include <geekos/mmio.h>

int Check_Timer(timerEvent event) {
	ulong_t currentTime = mmio_read(REG_TIMER_CURRENT);

	if(currentTime < event.endTime)
	{
		if(event.rollover)
			event.rollover;
	}
	else if(!event.rollover)
		return 1;

	return 0;
}

timerEvent Start_Timer(ulong_t usec) {
  timerEvent event;
  event.rollover = 0;
  event.endTime = 0;

  ulong_t currentTime = mmio_read(REG_TIMER_CURRENT);
  event.endTime = currentTime + usec;

  if(event.endTime > currentTime)
	    event.rollover = 0;
  else
	    event.rollover = 1;
  return event;
}

void Micro_Delay(int us) {
	timerEvent event = Start_Timer(us);
	while(!Check_Timer(event));
}
