/* A test program for semaphores
 * Copyright (c) 2001,2003,2004 David H. Hovemeyer <daveho@cs.umd.edu>
 * Copyright (c) 2003,2013,2014 Jeffrey K. Hollingsworth <hollings@cs.umd.edu>
 *
 * All rights reserved.
 *
 * This code may not be resdistributed without the permission of the copyright holders.
 * Any student solutions using any of this code base constitute derviced work and may
 * not be redistributed in any form.  This includes (but is not limited to) posting on
 * public forums or web sites, providing copies to (past, present, or future) students
 * enrolled in similar operating systems courses the University of Maryland's CMSC412 course.
 */

#include "libuser.h"
#include "conio.h"

int main(int argc __attribute__ ((unused)), char **argv
         __attribute__ ((unused))) {
    int semkey, result;

    PrintUser("Open_Semaphore()...\n");
    semkey = Open_Semaphore("semtest", 3);
    PrintUser("Open_Semaphore() returned %d\n", semkey);

    if(semkey < 0)
        return 0;

    PrintUser("P()...\n");
    result = P(semkey);
    PrintUser("P() returned %d\n", result);

    PrintUser("P()...\n");
    result = P(semkey);
    PrintUser("P() returned %d\n", result);

    PrintUser("V()...\n");
    result = V(semkey);
    PrintUser("V() returned %d\n", result);


    PrintUser("Close_Semaphore()...\n");
    result = Close_Semaphore(semkey);
    PrintUser("Close_Semaphore() returned %d\n", result);

    return 0;
}
