#include <conio.h>
#include <process.h>
#include <sched.h>
#include <sema.h>
#include <string.h>
#include <fileio.h>

int main(int argc, char *argv[]) {
    char command_line[256];
    int recursion_depth;
    int r;

    assert(argc > 0);
    if(argc == 1) {
        PrintUser("usage: execr1 <recursion depth>\n");
        Exit(-2);
    }
    assert(argv[1][0] >= '0' && argv[1][0] <= '9');
    recursion_depth = atoi(argv[1]);
    if(recursion_depth == 0) {
        PrintUser("done!\n");
        Exit(0);
    } else {
        PrintUser("%d...\n", recursion_depth);
        snprintf(command_line, 256, "execr1 %d", recursion_depth - 1);
        r = Execl("/c/execr1.exe", command_line);
        PrintUser("returned from Execl.  not good.\n");
        assert(0);
    }
    return 0;                   /* unnecessary */
}
