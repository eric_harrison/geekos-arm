/*
 * Program to write to LCD on RPI breadboard.
 */

#include <geekos/gpio.h>
#include <geekos/timer.h>
#include <geekos/mmio.h>
#include <conio.h>

/*
 * TODO:
 * 
 * Add some sort of command interpreter.
 *
 * Print word.
 *
 * Lookup table for characters.
 * Print character.
 * Display on/off.
 */

/* 
 * Configurable pin layout.
 * At the moment D0-D7 get mapped to 2-4, 7-11 and RS/RW get mapped to 23 & 24.
 */

#define SLEEPTIME 1000

/* RS, RW, ENABLE go to pins 23-25 */
#define LCD_SELECT 23
#define LCD_RW 24
#define LCD_ENABLE 25

/* Data bits are designed to go to pins 2, 3, 4, 7, 8, 9, 10, 11 */
uint_t get_gpio_pin(uint_t data_bit) {
	if(data_bit <= 2) {
		return data_bit + 2;
	} else {
		return data_bit + 4;
	}
}

void enable_gpio() {

	/* Set pins 2-4, 7-11, 23, 24 */
	unsigned char i;
	for(i = 2; i <= 4; i++) {
		gpio_function_select(i, GPIO_WR);
	}

	for(i = 7; i <= 9; i++) {
		gpio_function_select(i, GPIO_WR);
	}

	for(i = 10; i <= 11; i++) {
		gpio_function_select(i, GPIO_WR);
	}

	for(i = 23; i <= 25; i++) {
		gpio_function_select(i, GPIO_WR);
	}

}

void write_instruction(unsigned char select_signal, unsigned char rw_signal, unsigned char data) {

	unsigned int i;
	for(i = 0; i < 8; i++) {
		gpio_write(get_gpio_pin(i), data & (1 << i)); 
	}

	gpio_write(LCD_SELECT, select_signal);
	gpio_write(LCD_RW, rw_signal);

	/* Ensure GPIO has been stable long enough before sending command. */
	gpio_sync();
	Micro_Delay(SLEEPTIME);

	for(i = 0; i < 8; i++) {
		//PrintUser("Bit number %d is going to be %d\n\r", i, data & (1 << i));
		PrintUser("GPIO %d is %u\n\r", i, gpio_read(get_gpio_pin(i)));
	}

	PrintUser("SEL is %d\n\r", gpio_read(LCD_SELECT));
	PrintUser("RW is %d\n\r", gpio_read(LCD_RW));

	/* Write enable bit. */
	gpio_write(LCD_ENABLE, 0);

	/* Sleep. */
	gpio_sync();
	Micro_Delay(SLEEPTIME);
	PrintUser("Enable is %d\n\r", gpio_read(LCD_ENABLE));

	/* Disable */
	gpio_write(LCD_ENABLE, 1);

	gpio_sync();
	PrintUser("Enable is %d\n\r", gpio_read(LCD_ENABLE));

}

int main(int argc, char **argv) {

	enable_gpio();
	
	/* Clear enable bit. */
	gpio_write(LCD_ENABLE, 1);

	gpio_sync();
	Micro_Delay(SLEEPTIME);

	/* Send a clear display message. */

	/* Turn on display. */
	write_instruction(0, 0, 0x0f);

	/* Write a message. */
	//write_instruction(1, 0, 0x41);
	
	return 0;
}
