/*
 * Semaphores
 * Copyright (c) 2003, Jeffrey K. Hollingsworth <hollings@cs.umd.edu>
 * Copyright (c) 2004, David H. Hovemeyer <daveho@cs.umd.edu>
 * $Revision: 1.12 $
 * 
 * This is free software.  You are permitted to use,
 * redistribute, and modify it as specified in the file "COPYING".
 */

#include <geekos/syscall.h>
#include <geekos/sem.h>
#include <string.h>
#include <sema.h>

DEF_SYSCALL(Open_Semaphore_Inner, SYS_OPEN_SEMAPHORE, int,
            (const char *name, size_t len, int ival))

int Open_Semaphore(const char *name, int ival) {
	return Open_Semaphore_Inner(name, strlen(name), ival);
}

DEF_SYSCALL(P, SYS_P, int, (int s))
DEF_SYSCALL(V, SYS_V, int, (int s))
DEF_SYSCALL(Close_Semaphore, SYS_CLOSE_SEMAPHORE, int, (int s))
