/*
 * User File I/O
 * Copyright (c) 2003, Jeffrey K. Hollingsworth <hollings@cs.umd.edu>
 * Copyright (c) 2004, David H. Hovemeyer <daveho@cs.umd.edu>
 * $Revision: 1.29 $
 *
 * This is free software.  You are permitted to use,
 * redistribute, and modify it as specified in the file "COPYING".
 */

#include <geekos/errno.h>
#include <geekos/syscall.h>
#include <fileio.h>
#include <string.h>

DEF_SYSCALL(Stat_Inner, SYS_STAT, int,
            (const char *filename, size_t filenameLen, struct VFS_File_Stat * stat))

int Stat(const char *filename, struct VFS_File_Stat * stat) {
	return Stat_Inner(filename, strlen(filename), stat);
}

DEF_SYSCALL(FStat, SYS_FSTAT, int, (int fd, struct VFS_File_Stat * stat))

DEF_SYSCALL(Open_Inner, SYS_OPEN, int, (const char *filename, size_t filenameLen, int mode))

int Open(const char *filename, int mode) {
	return Open_Inner(filename, strlen(filename), mode);
}

DEF_SYSCALL(Create_Directory_Inner, SYS_CREATEDIR, int, (const char *dirname, size_t dirnameLen))

int Create_Directory(const char *dirname) {
	return Create_Directory_Inner(dirname, strlen(dirname));

}

DEF_SYSCALL(Open_Directory_Inner, SYS_OPENDIRECTORY, int, (const char *dirname, size_t dirnameLen))

int Open_Directory(const char *dirname) {
	return Open_Directory_Inner(dirname, strlen(dirname));
}

DEF_SYSCALL(Close, SYS_CLOSE, int, (int fd))
DEF_SYSCALL(Read_Entry, SYS_READENTRY, int,
                (int fd, struct VFS_Dir_Entry * entry))
DEF_SYSCALL(Read, SYS_READ, int, (int fd, void *buf, ulong_t len))
DEF_SYSCALL(Write, SYS_WRITE, int, (int fd, const void *buf, ulong_t len))
DEF_SYSCALL(Sync, SYS_SYNC, int, (void))
DEF_SYSCALL(Format_Inner, SYS_FORMAT, int,
                (const char *devname, size_t devnameLen, const char *fstype, size_t fstypeLen))

int Format(const char *devname, const char *fstype) {
	return Format_Inner(devname, strlen(devname), fstype, strlen(fstype));
}

DEF_SYSCALL(Seek, SYS_SEEK, int, (int fd, int pos))
DEF_SYSCALL(Delete_Inner, SYS_DELETE, int, (const char *path, size_t pathLen, bool recursive))

int Delete(const char *path, bool recursive) {
	return Delete_Inner(path, strlen(path), recursive);
}

DEF_SYSCALL(Rename_Inner, SYS_RENAME, int,
                (const char *oldpath, size_t oldpathLen, const char *newpath, size_t newpathLen))

int Rename(const char *oldpath, const char *newpath) {
	return Rename_Inner(oldpath, strlen(oldpath), newpath, strlen(newpath));
}

DEF_SYSCALL(Link_Inner, SYS_LINK, int,
                (const char *oldpath, size_t oldpathLen, const char *newpath, size_t newpathLen))

int Link(const char *oldpath, const char *newpath) {
	return Link_Inner(oldpath, strlen(oldpath), newpath, strlen(newpath));
}

DEF_SYSCALL(SymLink_Inner, SYS_SYMLINK, int,
                (const char *oldpath, size_t oldpathLen, const char *newpath, size_t newpathLen))

int SymLink(const char *oldpath, const char *newpath) {
	return SymLink_Inner(oldpath, strlen(oldpath), newpath, strlen(newpath));
}

DEF_SYSCALL(ReadBlock_Inner, SYS_READBLOCK, int,
                (const char *path, size_t pathLen, void *buf, unsigned int len,
                 unsigned int blocknum))

int ReadBlock(const char *path, void *buf, unsigned int len, unsigned int blocknum) {
	return ReadBlock_Inner(path, strlen(path), buf, len, blocknum);
}

DEF_SYSCALL(WriteBlock_Inner, SYS_WRITEBLOCK, int,
                (const char *path, size_t pathLen, void *buf, unsigned int len,
                 unsigned int blocknum))

int WriteBlock(const char *path, void *buf, unsigned int len, unsigned int blocknum) {
	return WriteBlock_Inner(path, strlen(path), buf, len, blocknum);
}

DEF_SYSCALL(PlaySoundFile_Inner, SYS_PLAY_SOUND_FILE, int, (const char *path, size_t pathLen))

int PlaySoundfile(const char *path) {
	return PlaySoundFile_Inner(path, strlen(path));
}

DEF_SYSCALL(Pipe, SYS_PIPE, int, (int *read_fd, int *write_fd))
DEF_SYSCALL(Diagnostic, SYS_DIAGNOSTIC, int, (void))

DEF_SYSCALL(Disk_Properties_Inner, SYS_DISKPROPERTIES, int,
                (const char *path, size_t pathLen, unsigned int *block_size,
                 unsigned int *blocks_on_disk))

int Disk_Properties(const char *path, unsigned int *block_size, unsigned int *blocks_on_disk) {
	return Disk_Properties_Inner(path, strlen(path), block_size, blocks_on_disk);
}

DEF_SYSCALL(SetAcl_Inner, SYS_SET_ACL, int,
                (const char *file, size_t fileLen, int uid, int permissions))

int SetAcl(const char *file, int uid, int permissions) {
	return SetAcl_Inner(file, strlen(file), uid, permissions);
}

DEF_SYSCALL(GetUid, SYS_GET_UID, int, ())
DEF_SYSCALL(SetEffectiveUid, SYS_SET_EFFECTIVE_UID, int, (int uid))
DEF_SYSCALL(SetSetUid_Inner, SYS_SET_SET_UID, int, (const char *file, size_t fileLen, int uid))

int SetSetUid(const char *file, int uid) {
	return SetSetUid_Inner(file, strlen(file), uid);
}

DEF_SYSCALL(Mmap, SYS_MMAP, int,
                (const void *addr, size_t length, int prot, int flags,
                 int fd))
DEF_SYSCALL(Munmap, SYS_MUNMAP, int, (const void *addr))

static bool Copy_String(char *dst, const char *src, size_t len) {
    if(strnlen(src, len) == len)
        return false;
    strcpy(dst, src);
    return true;
}

DEF_SYSCALL(Mount_Inner, SYS_MOUNT, int, (struct VFS_Mount_Request *argsPtr))

/*
 * The Mount() system call requires special handling because
 * its arguments are passed in a struct, since too many registers
 * would be required to pass the arguments entirely in
 * registers.
 */
int Mount(const char *devname, const char *prefix, const char *fstype) {
    struct VFS_Mount_Request args;

    if(!Copy_String(args.devname, devname, sizeof(args.devname)) ||
       !Copy_String(args.prefix, prefix, sizeof(args.prefix)) ||
       !Copy_String(args.fstype, fstype, sizeof(args.fstype)))
        return EINVALID;

		return Mount_Inner(&args);
}
