/*
 * Process creation and management
 * Copyright (c) 2003, Jeffrey K. Hollingsworth <hollings@cs.umd.edu>
 * Copyright (c) 2004, David H. Hovemeyer <daveho@cs.umd.edu>
 * $Revision: 1.21 $
 *
 * This is free software.  You are permitted to use,
 * redistribute, and modify it as specified in the file "COPYING".
 */

#include <stddef.h>
#include <geekos/ktypes.h>
#include <geekos/syscall.h>
#include <geekos/errno.h>
#include <string.h>
#include <process.h>
#include <conio.h>

/* System call wrappers */
DEF_SYSCALL(Null, SYS_NULL, int, (void))
DEF_SYSCALL(Exit, SYS_EXIT, int, (int exitCode))
DEF_SYSCALL(Spawn_Program_Inner, SYS_SPAWN, int,
			(const char *program, size_t programLen, const char *command, size_t commandLen,
			 int background))

int Spawn_Program(const char *program, const char *command, int background) {
	return Spawn_Program_Inner(program, strlen(program), command, strlen(command), background);
}

DEF_SYSCALL(Wait, SYS_WAIT, int, (int pid))
DEF_SYSCALL(Get_PID, SYS_GETPID, int, (void))
DEF_SYSCALL(PS, SYS_PS, int, (struct Process_Info * ptable, int len))

DEF_SYSCALL(WaitNoPID, SYS_WAITNOPID, int, (int *status))
DEF_SYSCALL(ShutDown, SYS_SHUTDOWN, int, (void))

DEF_SYSCALL(Fork, SYS_FORK, int, (void))
DEF_SYSCALL(Clone, SYS_CLONE, int,
			(void (*func) (void), void *stackPtr))

DEF_SYSCALL(Execl_Inner, SYS_EXECL, int,
			(const char *program, size_t programLen, const char *command, size_t commandLen))

int Execl(const char *program, const char *command) {
	return Execl_Inner(program, strlen(program), command, strlen(command));
}

	/* replace this by a real syscall */
	int Limit(int res, int limit) {
		PrintUser("ERROR: Placeholder Limit call\n");
		return -1;
	}

DEF_SYSCALL(Set_Affinity, SYS_SET_AFFINITY, int, (int pid, int aff))
DEF_SYSCALL(Get_Affinity, SYS_GET_AFFINITY, int, (int pid))

#define CMDLEN 79
static bool Ends_With(const char *name, const char *suffix) {
	size_t nameLen = strlen(name);
	size_t suffixLen = strlen(suffix);
	size_t start, i;

	if(suffixLen > nameLen)
		return false;
	start = nameLen - suffixLen;

	for(i = 0; i < suffixLen; ++i) {
		if(name[start + i] != suffix[i])
			return false;
	}
	return true;
}


int Spawn_With_Path(const char *program, const char *command,
		const char *path, int background) {
	int pid = -1;
	char exeName[(CMDLEN * 2) + 5];


	/* Try executing program as specified */
	pid = Spawn_Program(program, command, background);

	if(pid == ENOTFOUND && strchr(program, '/') == 0) {
		/* Search for program on path. */
		for(;;) {
			char *p;

			while (*path == ':')
				++path;

			if(strcmp(path, "") == 0)
				break;

			p = strchr(path, ':');
			if(p != 0) {
				memcpy(exeName, path, p - path);
				exeName[p - path] = '\0';
				path = p + 1;
			} else {
				strcpy(exeName, path);
				path = "";
			}

			strcat(exeName, "/");
			strcat(exeName, program);

			if(!Ends_With(exeName, ".exe"))
				strcat(exeName, ".exe");

			pid = Spawn_Program(exeName, command, background);
			if(pid != ENOTFOUND)
				break;
		}
	}

	return pid;
}
