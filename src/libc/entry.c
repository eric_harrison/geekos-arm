/*
 * User program entry point function
 * Copyright (c) 2003, Jeffrey K. Hollingsworth <hollings@cs.umd.edu>
 * Copyright (c) 2004, David H. Hovemeyer <daveho@cs.umd.edu>
 * $Revision: 1.14 $
 *
 * This is free software.  You are permitted to use,
 * redistribute, and modify it as specified in the file "COPYING".
 */

#include <geekos/argblock.h>
#include <signal.h>

int main(int argc, char **argv);
void Exit(int exitCode);

/*
 * Entry point.  Calls user program's main() routine, then exits.
 */
void _Entry(int argc, char ** argv) {

    /* Initialize the signal handling trampoline */
    {
        int ret = Sig_Init();
        if(ret != 0)
            Exit(ret);
    }

    /* Call main(), and then exit with whatever value it returns. */
    Exit(main(argc, argv));
}

void raise() {

	/* bad things happen */

}
