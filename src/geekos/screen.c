/*
 * Printing for the Raspberry Pi.
 */

#include <geekos/screen.h>
#include <geekos/uart.h>
#include <geekos/fmtout.h>
#include <geekos/ktypes.h>
#include <geekos/serial.h>
#include <geekos/vga.h>

extern struct Output_Sink g_serialOutputSink;
extern struct Output_Sink g_vgaOutputSink;

void Print(const char *fmt, ...) {
    va_list args;

    va_start(args, fmt);

#ifdef SERIAL_OUTPUT
    Format_Output(&g_serialOutputSink, fmt, args);
#endif

#ifdef VGA_OUTPUT
		Format_Output(&g_vgaOutputSink, fmt, args);
#endif

    va_end(args);
}

void PrintString(const unsigned char * buf, ulong_t length) {

#ifdef SERIAL_OUTPUT
	Serial_Print_String(buf, length);
#endif

#ifdef VGA_OUTPUT
	VGA_Print_String(buf, length);
#endif

}
