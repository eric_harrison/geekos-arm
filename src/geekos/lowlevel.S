# Low level interrupt/thread handling code for GeekOS.
# Copyright (c) 2001,2003,2004 David H. Hovemeyer <daveho@cs.umd.edu>
# Copyright (c) 2003,2014 Jeffrey K. Hollingsworth <hollings@cs.umd.edu>
# $Revision: 1.18 $

# All rights reserved.
# This code may not be resdistributed without the permission of the copyright holders.

# This is 32 bit code to be linked into the kernel.
# It defines low level interrupt handler entry points that we'll use
# to popregulate the IDT.  It also contains the interrupt handling
# and thread context switch code.

# ----------------------------------------------------------------------
# Definitions
# ----------------------------------------------------------------------

# Save registers prior to calling a handler function.
# This must be kept up to date with:
#   - Interrupt_State struct in int.h
#   - Setup_Kernel_Thread(), Setup_User_Thread() in kthread.c
#   - INTERRUPT_STATE_SIZE above (count of registers pushreged * 4) 

# Stack to use for non-SVC interrupts (abort, IRQ)
.equ IRQ_STACK, 0x8000

.macro _memory_barrier
	mov r1, #0
	mcr p15, #0, r1, c7, c10, #5
.endm

.macro _Save_Registers
stmfd sp, {lr}
mov lr, sp
sub lr, #4
stmfd lr, {r0-r14}
sub lr, #60
mrs sp, cpsr
stmfd lr!, {sp}
.endm

.macro _Save_User_Registers
	stmfd sp!, {lr}
	stmfd sp, {r0-r14}^
	sub sp, #60     // 15 registers * 4 bytes each = 60, ARM compiler complains about unpredictable SP otherwise.
	mrs r0, spsr
	stmfd sp!, {r0}
.endm

.macro _Restore_SVC_Registers
ldmfd sp, {r0-r14, pc}^
.endm

.macro _Restore_User_Registers
ldmfd sp, {r0-r14}^
add sp, #60
ldmfd sp, {pc}^
.endm

.macro _Restore_User_Mode
ldmfd sp!, {r0}
msr spsr, r0
_Restore_User_Registers
.endm

.macro _Restore_SVC_Mode
ldmfd sp!, {r0}
msr spsr, r0
_Restore_SVC_Registers
.endm

.macro _Restore_Registers
// Pop spsr off of stack
ldmfd sp!, {r0}
msr spsr, r0
mov r1, r0

// Check whether user or SVC mode
	and r1, #0x1f
	mov r2, #0x10
	teq r1, r2

	beq 1f
	// SVC mode restore
	_Restore_SVC_Registers
1:	
	// User mode restore
	_Restore_User_Registers
.endm

# Code to activate a new user context (if necessary), before returning
# to executing a thread.  Should be called just before restoring
# registers (because the interrupt context is used).
.macro Activate_User_Context
	# If the new thread has a user context which is not the current
	# one, activate it.
	mov r1, sp											// Interrupt_State pointer
	Get_Current_Thread_To_EAX
	ldr pc, =Switch_To_User_Context
.endm

.macro Handle_User_Exception
	ldr sp, =g_currentThreads				// Load kernel stack pointer for current thread
	ldr sp, [sp, #0]								// Load address of current thread
	ldr sp, [sp, #0]								// Load address of current thread stack pointer

	_Save_User_Registers

	ldr lr, =Restore_User_Mode

	mov r0, sp 											// Pass interrupt state as first argument
.endm

# Macro for exceptions not occurring from user mode
.macro Handle_Nonuser_Exception
	// Need to store link register and spsr somewhere - use small IRQ Stack
	ldr sp, =IRQ_STACK
	stmdb sp!, {lr}
	mrs lr, spsr
	stmdb sp, {lr}
	
	// Swap to SVC mode to access PCB stack
	mrs lr, cpsr_all
	bic lr, lr, #0x1f
	orr lr, lr, #0x13
	msr cpsr_all, lr

	// Come back to store link register later.
	stmdb sp, {lr}
	mov lr, sp
	sub lr, #8
	stmfd lr, {r0-r13}
	ldmdb sp, {lr}

	// Acquire pointer to IRQ Stack and get saved lr/spsr
	ldr r0, =IRQ_STACK
	ldmdb r0, {r1, r2}

	// Push lr and banked lr onto stack at bottom
	// It's not clear why, but using these two registers in the same push instruction causes lr to never get written.
	stmfd sp!, {r2}
	stmfd sp!, {lr}

	// Push saved spsr onto stack (it goes at the top)
	sub sp, #56
	stmfd sp!, {r1}

	mov r0, sp
	ldr lr, =Restore_SVC_Mode
.endm

.macro Handle_Exception
	// Store banked link register on IRQ stack, and use it to check mode
	ldr sp, =IRQ_STACK
	stmdb sp!, {lr}
	mrs lr, spsr
	and lr, #0x1f
	mov sp, #0x10
	teq sp, lr
	// Restore LR
	ldr sp, =IRQ_STACK
	ldmdb sp, {lr}

	beq 1f
	// If previous mode was not user mode, use Handle_Exception
	Handle_Nonuser_Exception
	b 2f
1:	
	// If previous mode was user mode, use Handle_User_Excpetion
	Handle_User_Exception
2:
.endm

# ----------------------------------------------------------------------
# Symbol imports and exports
# ----------------------------------------------------------------------

# Global variable pointing to context struct for current thread.
.extern g_currentThreads

# Set to non-zero when we need to choose a new thread
# in the interrupt return code.
.extern g_needReschedule

# This is the function that returns the next runnable thread.
.extern Get_Next_Runnable

# Function to put a thread on the run queue.
.extern Make_Runnable

# Function to activate a new user context (if needed).
.extern Switch_To_User_Context

# Function that checks if the current thread has a signal pending.
.extern Check_Pending_Signal

# Function that sets up the stack frame to invoke a signal handler
.extern Setup_Frame

# Thread context switch function.
.global Switch_To_Thread

# Virtual memory support.
.global Enable_Paging
.global Set_PDBR
.global Get_PDBR
.global Flush_TLB

# Interrupt Handlers
.extern Reset_Handler
.extern SVC_Handler
.extern Undefined_Handler
.extern Abort_Handler
.extern IRQ_Handler

# Spin Lock
.global Spin_Lock_INTERNAL
.global Spin_Unlock_INTERNAL

.global memory_barrier
.global Get_CPSR
.global Interrupt_Vector_Table

# ----------------------------------------------------------------------
# Code
# ----------------------------------------------------------------------

.text

#
# Start paging
.align 8
Enable_Paging:
	// Load TTBR0
	mcr p15, 0, r0, c2, c0, 0

	// Set domain access control to manager
	mov r0, #0x3
	mcr p15, 0, r0, c3, c0, 0

	// Enable paging bit in MMU
	mrc p15, 0, r0, c1, c0, 0
	orr r0, r0, #0x1
	mcr p15, 0, r0, c1, c0, 0

	mov pc, lr

#
# Change PDBR 
#	load cr3 with the passed page directory pointer
.align 8
Set_PDBR:
	mcr p15, 0, r0, c2, c0, 0
	mov pc, lr

#
# Get the current PDBR.
# This is useful for lazily switching address spaces#
# only switch if the new thread has a different address space.
#
.align 8
Get_PDBR:
	mrc p15, 0, r0, c2, c0, 0
	mov pc, lr

#
# Flush TLB - just need to re-load cr3 to force this to happen
#
# - XXXX do we need to shoot down the TLBs on other cores with an IPI??
#   - might need to just make sure we don't take pages from other processes, since the TLB clearing is currently
#     only used to clear the TLB when we take page out a page from a process, need to ensure the TLB entry (if any) 
#     for it is cleared.  When we start to have shared memory between cores, will need to look at a full shootdown
#     strategy.
#
.align 8
Flush_TLB:
	mcr p15,0,r0,c8, c5, 0
	mov pc, lr

# eax = &g_currentThreads[Get_CPU_ID()]#
.macro Mov_EAX_Current_Thread_PTR
	ldr r0, =g_currentThreads
.endm

## eax = *( &g_currentThreads[Get_CPU_ID()] )
.macro Get_Current_Thread_To_EAX
	Mov_EAX_Current_Thread_PTR
	ldr	r0, [r0,#0]              
.endm

.macro Set_Current_Thread_From_EBX
	Mov_EAX_Current_Thread_PTR
	str r1, [r0]
.endm

# ----------------------------------------------------------------------
# Switch_To_Thread()
#   Save context of currently executing thread, and activate
#   the thread whose context object is passed as a parameter.
# 
# Parameter: 
#   - ptr to Kernel_Thread whose state should be restored and made active
#
# Notes:
# Called with interrupts disabled.
# This must be kept up to date with definition of Kernel_Thread
# struct, in kthread.h.
# ----------------------------------------------------------------------
.align 8
Switch_To_Thread:

	# Store current interrupt state on stack
	_Save_Registers

	# Set esp in kthread struct
	ldr sp, =g_currentThreads				// Load kernel stack pointer for current thread
	ldr sp, [sp, #0]								// Load address of current thread
	str lr, [sp, #0]								// Store address of current thread stack pointer

	# Make new thread current
	ldr sp, =g_currentThreads
	str r0, [sp, #0]

	# Swap to new thread's stack
	ldr sp, [r0]

	# Activate the user context, if necessary.
	ldr lr, =End_Switch_Thread
	ldr pc, =Switch_To_User_Context

.align 8
End_Switch_Thread:
	ldr lr, =Restore_Registers
	ldr pc, =unlockKernel

.align 8
Restore_Registers:
	_Restore_Registers

.align 8
Restore_SVC_Mode:
	_Restore_SVC_Mode

.align 8
Restore_User_Mode:
	_Restore_User_Mode

.align 8
_Undefined_Handler:
	Handle_Exception
	ldr pc, =Undefined_Handler

.align 8
_SVC_Handler:
	Handle_User_Exception
	
	# Call interrupt handler
	ldr pc, =SVC_Handler

.align 8
_Abort_Handler:
	sub lr, lr, #8
	Handle_Exception
	ldr pc, =Abort_Handler

.align 8
_IRQ_Handler:
	sub lr, lr, #4
	Handle_Exception
	ldr pc, =IRQ_Handler

.align 8
memory_barrier:
	_memory_barrier
	mov pc, lr

.align 8
Spin_Lock_INTERNAL:
get_lock:
	// Raspberry pi goes kaboom when attempting LDREX and STREX here with MMU off.
	LDR   	r1, [r0]
	CMP     r1, #0
	BNE     get_lock

	MOV     r1, #1
	STR   	r1, [r0]
	// CMP     r2, #0x0
	// BNE     Spin_Lock_INTERNAL
	mov pc, lr

.align 8
Spin_Unlock_INTERNAL:
	_memory_barrier
	MOV     r1, #0
	STR     r1, [r0]
	mov pc, lr

.align 8
Get_CPSR:
	mrs r0, cpsr
	mov pc, lr

.data
.section ".data.local.ivt"
// The two extra no-ops at the beginning allow the IVT to be located at address 0x8008,
// hence allowing a relative jump of 0x8000 to be made when an interrupt happens ("add pc" will add to the pc, but increment pc by 8 at end)
Local_Interrupt_Vector_Table:
	nop
	nop
	ldr pc, =_start
	ldr pc, =_Undefined_Handler
	ldr pc, =_SVC_Handler
	ldr pc, =_Abort_Handler
	ldr pc, =_Abort_Handler
	nop
	ldr pc, =_IRQ_Handler

.section ".data.ivt"
Interrupt_Vector_Table:
	add pc, pc, #0x8000
	add pc, pc, #0x8000
	add pc, pc, #0x8000
	add pc, pc, #0x8000
	add pc, pc, #0x8000
	add pc, pc, #0x8000
	add pc, pc, #0x8000
