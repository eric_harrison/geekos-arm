/*
 * Copyright (c) 2013,2014 Jeffrey K. Hollingsworth <hollings@cs.umd.edu>
 *
 * All rights reserved.
 *
 * This code may not be resdistributed without the permission of the copyright holders.
 * Any student solutions using any of this code base constitute derviced work and may
 * not be redistributed in any form.  This includes (but is not limited to) posting on
 * public forums or web sites, providing copies to (past, present, or future) students
 * enrolled in similar operating systems courses the University of Maryland's CMSC412 course.
 */

#include <geekos/string.h>
#include <geekos/screen.h>
#include <geekos/kassert.h>
#include <geekos/smp.h>
#include <geekos/list.h>
#include <geekos/int.h>
#include <geekos/idt.h>
#include <geekos/malloc.h>
#include <geekos/kthread.h>
#include <geekos/timer.h>
#include <geekos/trap.h>
#include <geekos/mem.h>
#include <geekos/kassert.h>

static Spin_Lock_t globalLock;

int CPU_Count;

/* Raspberry Pi single core support */
int Get_CPU_ID(void) {
	return 0; 
}

CPU_Info CPUs[MAX_CPUS];

/* 
 * START_SECONDARY_FUNC Must be updated if you change:
 *		STARTSEG in defs.asm
 *		alignment before start_secondary_cpu in setup.asm 
 * SECONDARY_STACK is the address to store the initial value of the stack
 */

// global spin lock for all list operations
Spin_Lock_t listLock;

Spin_Lock_t kthreadLock;

int Is_Locked(Spin_Lock_t * lock) {
    return lock->lock;
}

void Spin_Lock_Init(Spin_Lock_t * lock) {
    KASSERT(lock);

    lock->lock = 0;
    lock->lastLocker = NULL;
    lock->locker = NULL;
}

int Try_Spin_Lock(Spin_Lock_t * lock) {
	// TODO: this isn't quite right yet.
	Spin_Lock(lock);
	return 1;
}

extern void Spin_Lock_INTERNAL(Spin_Lock_t * lock);

void Spin_Lock(Spin_Lock_t * lock) {
    struct Kernel_Thread *current = get_current_thread(0);      /* don't want to disable interrupts 
                                                                   for an advisory variable. */
//		Print("Spin Locking lock %p; current thread is %p\n\r", lock, current);
    KASSERT(lock);
    Spin_Lock_INTERNAL(lock);
    lock->locker = current;
}

extern void Spin_Unlock_INTERNAL(Spin_Lock_t * lock);

void Spin_Unlock(Spin_Lock_t * lock) {
//	Print("Spin UNLOCKing %p; current thread is %p\n\r", lock, get_current_thread(0));
	KASSERT(lock);
	KASSERT(lock->lock);
	lock->lastLocker = lock->locker;
	lock->locker = (struct Kernel_Thread *) 0xdead1000; /* invalid */
	Spin_Unlock_INTERNAL(lock);
}

/*
 * Implement coarse grained kernel locks.  The functions lockKernel and unlockKernel will be called whenever
 *   interrupts are enabled or disabled.  This includes in interrupt and iret (see lowlevel.asm).
 */

void lockKernel() {
	Spin_Lock(&globalLock);
}

void unlockKernel() {
	Spin_Unlock(&globalLock);
}

bool Kernel_Is_Locked(void) {
    return (globalLock.lock);
}


struct Kernel_Thread *get_current_thread(int atomic) {
    int i = atomic ? Begin_Int_Atomic() : 0;    /* an interrupt could break us between the cpuid get and the subscript */
    struct Kernel_Thread *ret = g_currentThreads[Get_CPU_ID()];
    if(atomic)
        End_Int_Atomic(i);
    return ret;
}
