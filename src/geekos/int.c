/*
 * GeekOS interrupt handling data structures and functions
 * Copyright (c) 2001,2003 David H. Hovemeyer <daveho@cs.umd.edu>
 * $Revision: 1.18 $
 * 
 * This is free software.  You are permitted to use,
 * redistribute, and modify it as specified in the file "COPYING".
 */

#include <geekos/idt.h>         /* x86-specific int handling stuff */
#include <geekos/screen.h>
#include <geekos/kassert.h>
#include <geekos/paging.h>
#include <geekos/string.h>
#include <geekos/int.h>
#include <geekos/mmio.h>

/* ----------------------------------------------------------------------
 * Private functions and data
 * ---------------------------------------------------------------------- */

/*
 * A dummy interrupt handler function.
 * Ensures that the low-level interrupt code always
 * has a handler to call.
 */
void Dummy_Interrupt_Handler(struct Interrupt_State *state) {
    Print("*** Unexpected interrupt! ***\n");
    Dump_Interrupt_State(state);
    STOP();
}

static void Print_Selector(const char *regName, uint_t value)
    __attribute__ ((unused));
static void Print_Selector(const char *regName, uint_t value) {
    Print("%s: index=%d, ti=%d, rpl=%d\n",
          regName, value >> 3, (value >> 2) & 1, value & 3);
}

/* ----------------------------------------------------------------------
 * Public functions
 * ---------------------------------------------------------------------- */

extern char Interrupt_Vector_Table;
void Init_Interrupts(int cpuID) {

	Print("IVT is at addr %p\n\r", &Interrupt_Vector_Table);

	/* Copy over IVT, which goes at address zero. */
	memcpy((void *) 0, &(Interrupt_Vector_Table), 28);

	/* Enable secondary passthrough pin for secondary interrupts... */
	//mmio_setbit(REG_PIC_ENABLE, PIC_SIC_PIN);

	__Enable_Interrupts();
	KASSERT(Interrupts_Enabled());

}

/*
 * Query whether or not interrupts are currently enabled.
 */
extern ulong_t Get_CPSR();

bool Interrupts_Enabled(void) {
	/* Defined in lowlevel.S - queries the CPSR */
	ulong_t cpsr = Get_CPSR();
	return !(cpsr & (1 << CPSR_INTDISABLE));
}

/*
 * Dump interrupt state struct to screen
 */
void Dump_Interrupt_State(struct Interrupt_State *state) {

		Print("r0=%08x, r1=%08x, r2=%08x, r3=%08x\n\r"
				"r4=%08x, r5=%08x, r6=%08x, r7=%08x\n\r"
				"r8=%08x, r9=%08x, r10=%08x, r11=%08x\n\r"
				"r12=%08x, sp=%08x, lr=%08x, pc=%08x\n\r",
				state->r0, state->r1, state->r2, state->r3,
				state->r4, state->r5, state->r6, state->r7,
				state->r8, state->r9, state->r10, state->r11,
				state->r12, state->sp, state->lr, state->pc);

}
