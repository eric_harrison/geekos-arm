/*
 * Paging (virtual memory) support
 * Copyright (c) 2001,2003,2004 David H. Hovemeyer <daveho@cs.umd.edu>
 * Copyright (c) 2003,2013,2014 Jeffrey K. Hollingsworth <hollings@cs.umd.edu>
 *
 * All rights reserved.
 *
 * This code may not be resdistributed without the permission of the copyright holders.
 * Any student solutions using any of this code base constitute derviced work and may
 * not be redistributed in any form.  This includes (but is not limited to) posting on
 * public forums or web sites, providing copies to (past, present, or future) students
 * enrolled in similar operating systems courses the University of Maryland's CMSC412 course.
 *
 * $Revision: 1.56 $
 * 
 */

#include <geekos/string.h>
#include <geekos/int.h>
#include <geekos/idt.h>
#include <geekos/kthread.h>
#include <geekos/kassert.h>
#include <geekos/screen.h>
#include <geekos/mem.h>
#include <geekos/malloc.h>
#include <geekos/user.h>
#include <geekos/vfs.h>
#include <geekos/crc32.h>
#include <geekos/paging.h>
#include <geekos/errno.h>
#include <geekos/projects.h>
#include <geekos/smp.h>

#include <libc/mmap.h>

/* ----------------------------------------------------------------------
 * Public data
 * ---------------------------------------------------------------------- */

/* ----------------------------------------------------------------------
 * Private functions/data
 * ---------------------------------------------------------------------- */

#define SECTORS_PER_PAGE (PAGE_SIZE / SECTOR_SIZE)

/*
 * flag to indicate if debugging paging code
 */
int debugFaults = 0;
#define Debug(args...) if (debugFaults) Print(args)

/* Private Kernel Page directory. */
static pde_t * kernelDirectory;

/* const because we do not expect any caller to need to
   modify the kernel page directory */
const pde_t *Kernel_Page_Dir(void) {
    TODO_P(PROJECT_VIRTUAL_MEMORY_A, "return kernel page directory");
    return NULL;
}



/*
 * Print diagnostic information for a page fault.
 */
static void Print_Fault_Info(uint_t address, faultcode_t faultCode) {
    extern uint_t g_freePageCount;
    struct Kernel_Thread *current = get_current_thread(0);      /* informational, could be incorrect with low probability */

    if(current) {
        Print("Pid %d: (%p/%s)", current->pid, current,
              current->threadName);
    }
    Print("\n Page Fault received, at address %p (%d pages free)\n",
          (void *)address, g_freePageCount);
    if(faultCode.protectionViolation)
        Print("   Protection Violation, ");
    else
        Print("   Non-present page, ");
    if(faultCode.writeFault)
        Print("Write Fault, ");
    else
        Print("Read Fault, ");
    if(faultCode.userModeFault)
        Print("in User Mode\n");
    else
        Print("in Supervisor Mode\n");
}


/*
 * Handler for page faults.
 * You should call the Install_Interrupt_Handler() function to
 * register this function as the handler for interrupt 14.
 */
/*static*/ void Page_Fault_Handler(struct Interrupt_State *state) {
    ulong_t address;
		faultcode_t faultCode = Get_Page_Fault_Code();

    KASSERT(!Interrupts_Enabled());

    /* Get the address that caused the page fault */
    address = Get_Page_Fault_Address();
    Debug("Page fault @%lx\n", address);

    if(address < 0xfec01000 && address > 0xf0000000) {
        Print("page fault address in APIC/IOAPIC range\n");
        goto error;
    }

    /* rest of your handling code here */
    TODO_P(PROJECT_VIRTUAL_MEMORY_B, "handle page faults");

    TODO_P(PROJECT_MMAP, "handle mmap'd page faults");


  error:
    Print("Unexpected Page Fault received\n");
    Print_Fault_Info(address, faultCode);
    Dump_Interrupt_State(state);
    /* user faults just kill the process; not user mode faults should halt the kernel. */
    //KASSERT0(faultCode.userModeFault,
    //         "unhandled kernel-mode page fault.");

    /* For now, just kill the thread/process. */
    Enable_Interrupts();
    Exit(-1);
}

void Identity_Map_Page(pde_t * currentPageDir, unsigned int address,
                       int flags) {
}

/* ----------------------------------------------------------------------
 * Public functions
 * ---------------------------------------------------------------------- */

/* Map a block of memory into a page directory */
int Map_Pages(pde_t * directory, ulong_t physicalAddr, ulong_t virtualAddr, ulong_t numBytes, bool protected) {
#if 0
	KASSERT(numBytes % PAGE_SIZE == 0);
	KASSERT(physicalAddr % PAGE_SIZE == 0);
	KASSERT(virtualAddr % PAGE_SIZE == 0);

	char protected_bits = 0x3;
	if(protected) {
		protected_bits = 0x1;
	}

	/* Iterate through each page directory entry */
	ulong_t pde_index = PAGE_DIRECTORY_INDEX(virtualAddr);
	ulong_t pde_index_end = PAGE_DIRECTORY_INDEX(virtualAddr + numBytes);
	ulong_t currentAddrOffset = 0;

	for(; pde_index <= pde_index_end; pde_index++) {
		/* Check if there's a page table for this directory entry already. If not then allocate memory for it and set the pde descriptor */
		if((ulong_t) directory[pde_index] == 0) {

			pte_t * pageTableAddr = Malloc(sizeof(struct pte_t) * NUM_PAGE_TABLE_ENTRIES);
			if(directory[pde_index] == 0) {
				return ENOMEM;
			}

			/* Setup the directory entry fields */
			memset(directory[pde_index], 0, sizeof(struct pte_t) * NUM_PAGE_TABLE_ENTRIES);

			directory[pde_index].mode = 0x1;
			directory[pde_index].present = 0x1;
			directory[pde_index].pageTableBaseAddr = pageTableAddr;

		}

		/* Iterate through each page table entry */
		ulong_t pte_index = PAGE_TABLE_INDEX(currentAddr);
		ulong_t pte_index_end;

		/* Stop once we reach the last page. */
		if(currentAddr + sizeof(struct pte_t) * NUM_PAGE_TABLE_ENTRIES > virtualAddr + numBytes) {
			pte_index_end = PAGE_TABLE_INDEX(virtualAddr + numBytes);
		} else {
			pte_index_end =	PAGE_TABLE_INDEX(currentAddr + sizeof(struct pte_t) * NUM_PAGE_TABLE_ENTRIES);
		}

		for(pte_index; pte_index < pte_index_end; pte_index++, currentAddr += PAGE_SIZE) {

			/* Setup the page table entry fields */
			directory[pde_index][pte_index].mode = 0x2;
			directory[pde_index][pte_index].AP0 = protected_bits;
			directory[pde_index][pte_index].AP1 = protected_bits;
			directory[pde_index][pte_index].AP2 = protected_bits;
			directory[pde_index][pte_index].AP3 = protected_bits;
			directory[pde_index][pte_index].pageBaseAddr = currentAddr >> 12;

		}
	}
#endif
	return 0;
}

/* Initializes the page directory for a given user thread */
int Init_Page_Directory(struct Kernel_Thread * thread) {
#if 0
	pde_t * dir = Malloc(sizeof(pde_t) * NUM_DIRECTORY_ENTRIES);

	/* Insert kernel pages into directory */
	memcpy(dir, kernelDirectory, sizeof(pde_t) * NUM_DIRECTORY_ENTRIES);

	/* Map user address space */
	Map_Pages(dir, thread->userContext->memory, 0, thread->userContext->size, false);
#endif
	return 0;
}

/* Defined by linker */
extern char __end;

/*
 * Initialize virtual memory by building page tables
 * for the kernel and physical memory.
 */
void Init_VM(struct Boot_Info *bootInfo) {
#if 0
	kernelDirectory = Malloc(sizeof(pde_t *) * NUM_DIRECTORY_ENTRIES);
	memset(kernelDirectory, 0, sizeof(pde_t *) * NUM_DIRECTORY_ENTRIES);

	/* Map physical memory */

	ulong_t kernEnd = Round_Up_To_Page((int)&__end);
	pageListAddr = (kernEnd + KERNEL_HEAP_SIZE);

	ulong_t numPages = bootInfo->memSizeKB >> 2;
	ulong_t numPageListBytes = sizeof(struct Page) * numPages;
	pageListEnd = Round_Up_To_Page(pageListAddr + numPageListBytes);

	/* Map 0x00000000 into kernel for interrupt handling - yes, due to ARM design this can't be used for null pointer handling */
	Map_Pages_Protected(kernelDirectory, 0, 0, PAGE_SIZE, true);
	Map_Pages_Protected(kernelDirectory, KERN_THREAD_OBJ, KERN_THREAD_OBJ, pageListEnd - KERN_THREAD_OBJ, true);
	Map_Pages_Protected(kernelDirectory, BIOS_START, BIOS_START, BIOS_END - BIOS_START, true);

	Enable_Paging();
#endif
    TODO_P(PROJECT_VIRTUAL_MEMORY_A,
				           "Build initial kernel page directory and page tables");
}

void Init_Secondary_VM() {
    TODO_P(PROJECT_VIRTUAL_MEMORY_A, "enable paging on secondary cores");
}

/**
 * Initialize paging file data structures.
 * All filesystems should be mounted before this function
 * is called, to ensure that the paging file is available.
 */
void Init_Paging(void) {
	Install_Abort_Handler(&Page_Fault_Handler);
}

/**
 * Find a free bit of disk on the paging file for this page.
 * Interrupts must be disabled.
 * @return index of free page sized chunk of disk space in
 *   the paging file, or -1 if the paging file is full
 */
int Find_Space_On_Paging_File(void) {
    KASSERT(!Interrupts_Enabled());
    TODO_P(PROJECT_VIRTUAL_MEMORY_B, "Find free page in paging file");
    return EUNSUPPORTED;
}

/**
 * Free a page-sized chunk of disk space in the paging file.
 * Interrupts must be disabled.
 * @param pagefileIndex index of the chunk of disk space
 */
void Free_Space_On_Paging_File(int pagefileIndex) {
    /* KASSERT(!Interrupts_Enabled()); seems unnecessary - ns */
    TODO_P(PROJECT_VIRTUAL_MEMORY_B, "Free page in paging file");
}

/**
 * Write the contents of given page to the indicated block
 * of space in the paging file.
 * @param paddr a pointer to the physical memory of the page
 * @param vaddr virtual address where page is mapped in user memory
 * @param pagefileIndex the index of the page sized chunk of space
 *   in the paging file
 */
void Write_To_Paging_File(void *paddr, ulong_t vaddr, int pagefileIndex) {
    struct Page *page = Get_Page((ulong_t) paddr);
    KASSERT(!(page->flags & PAGE_PAGEABLE));    /* Page must be pageable! */
    KASSERT(page->flags & PAGE_LOCKED); /* Page must be locked! */
    TODO_P(PROJECT_VIRTUAL_MEMORY_B, "Write page data to paging file");
}

/**
 * Read the contents of the indicated block
 * of space in the paging file into the given page.
 * @param paddr a pointer to the physical memory of the page
 * @param vaddr virtual address where page will be re-mapped in
 *   user memory
 * @param pagefileIndex the index of the page sized chunk of space
 *   in the paging file
 */
void Read_From_Paging_File(void *paddr, ulong_t vaddr, int pagefileIndex) {
    struct Page *page = Get_Page((ulong_t) paddr);
    KASSERT(!(page->flags & PAGE_PAGEABLE));    /* Page must be locked! */
    TODO_P(PROJECT_VIRTUAL_MEMORY_B, "Read page data from paging file");
}


void *Mmap_Impl(void *ptr, unsigned int length, int prot, int flags,
                int fd) {
    TODO_P(PROJECT_MMAP, "Mmap setup mapping");
    return NULL;
}

bool Is_Mmaped_Page(struct User_Context * context, ulong_t vaddr) {
    TODO_P(PROJECT_MMAP,
           "is this passed vaddr an mmap'd page in the passed user context");
    return false;
}

void Write_Out_Mmaped_Page(struct User_Context *context, ulong_t vaddr) {
    TODO_P(PROJECT_MMAP, "Mmap write back dirty mmap'd page");
}

int Munmap_Impl(ulong_t ptr) {
    TODO_P(PROJECT_MMAP, "unmapp the pages");
    return 0;
}
