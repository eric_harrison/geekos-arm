/*
 * Trap handlers
 * Copyright (c) 2001,2003,2004 David H. Hovemeyer <daveho@cs.umd.edu>
 * Copyright (c) 2003,2013,2014 Jeffrey K. Hollingsworth <hollings@cs.umd.edu>
 *
 * All rights reserved.
 *
 * This code may not be resdistributed without the permission of the copyright holders.
 * Any student solutions using any of this code base constitute derviced work and may
 * not be redistributed in any form.  This includes (but is not limited to) posting on
 * public forums or web sites, providing copies to (past, present, or future) students
 * enrolled in similar operating systems courses the University of Maryland's CMSC412 course.

 * $Revision: 1.21 $
 * 
 */

#include <geekos/idt.h>
#include <geekos/kthread.h>
#include <geekos/defs.h>
#include <geekos/syscall.h>
#include <geekos/trap.h>
#include <geekos/user.h>
#include <geekos/projects.h>
#include <geekos/smp.h>

/*
 * TODO: need to add handlers for other exceptions (such as bounds
 * check, debug, etc.)
 */

/*
 * Handler for general protection faults and other bad errors.
 * Kill the current thread (which caused the fault).
 */
static void GPF_Handler(struct Interrupt_State *state) {
    struct Kernel_Thread *current = CURRENT_THREAD;
    /* Send the thread to the reaper... */
    Print("Exception received, killing thread %p (pid %d)\n",
          current, current->pid);
    Dump_Interrupt_State(state);

    if((state->r11 & ~0xfff) != (unsigned long)current->stackPage) {
        Print("ebp %x is not on the current kernel stack %p\n",
              state->r11, current->stackPage);
        /* KASSERT(false); */
    }

    Enable_Interrupts();        /* Exit will expects interrupts to be enabled. */
    Exit(-1);

    /* We will never get here */
    KASSERT(false);
}

static void Syscall_Handler(struct Interrupt_State * state) {
	
	/* Lookup interrupt number in syscall table */
	if(g_syscallTable[state->r7] == NULL) {

		/* Error: syscall not found */
		Print("Illegal system call %d by process %d\n",
				state->r7, CURRENT_THREAD->pid);
		Enable_Interrupts();
		Exit(-1);

	} else {
		state->r0 = g_syscallTable[state->r7](state);
	}

}

void Init_Traps() {
	Install_SVC_Handler(&Syscall_Handler);
	Install_Undefined_Handler(&GPF_Handler);
}
