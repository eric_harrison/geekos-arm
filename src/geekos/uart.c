/*
 * UART Functions
 * Copyright (c) 2001,2003,2004 David H. Hovemeyer <daveho@cs.umd.edu>
 * Copyright (c) 2003,2013,2014 Jeffrey K. Hollingsworth <hollings@cs.umd.edu>
 *
 * All rights reserved.
 *
 * This code may not be resdistributed without the permission of the copyright holders.
 * Any student solutions using any of this code base constitute derviced work and may
 * not be redistributed in any form.  This includes (but is not limited to) posting on
 * public forums or web sites, providing copies to (past, present, or future) students
 * enrolled in similar operating systems courses the University of Maryland's CMSC412 course.
 *
 */

/* Code adapted from http://wiki.osdev.org/Raspberry_Pi_Bare_Bones */

#include <geekos/uart.h>
#include <geekos/mmio.h>
#include <geekos/idt.h>
#include <geekos/keyboard.h>
#include <geekos/defs.h>
#include <geekos/string.h>

#define GPPUD (REG_GPIO + 0x94)
#define GPPUDCLK0 (REG_GPIO + 0x98)
#define	UART0_DR      (REG_UART0 + 0x00)
#define	UART0_RSRECR  (REG_UART0 + 0x04)
#define	UART0_FR      (REG_UART0 + 0x18)
#define	UART0_ILPR    (REG_UART0 + 0x20)
#define	UART0_IBRD    (REG_UART0 + 0x24)
#define	UART0_FBRD    (REG_UART0 + 0x28)
#define	UART0_LCRH    (REG_UART0 + 0x2C)
#define	UART0_CR      (REG_UART0 + 0x30)
#define	UART0_IFLS    (REG_UART0 + 0x34)
#define	UART0_IMSC    (REG_UART0 + 0x38)
#define	UART0_RIS     (REG_UART0 + 0x3C)
#define	UART0_MIS     (REG_UART0 + 0x40)
#define	UART0_ICR     (REG_UART0 + 0x44)
#define	UART0_DMACR   (REG_UART0 + 0x48)
#define	UART0_ITCR    (REG_UART0 + 0x80)
#define	UART0_ITIP    (REG_UART0 + 0x84)
#define	UART0_ITOP    (REG_UART0 + 0x88)
#define	UART0_TDR     (REG_UART0 + 0x8C)

/* Loop <delay> times in a way that the compiler won't optimize away. */
static inline void delay(ulong_t count)
{
	asm volatile("__delay_%=: subs %[count], %[count], #1; bne __delay_%=\n"
			: : [count]"r"(count) : "cc");
}

/* Interrupt Handler for UART0 Pin on PIC */
void UART_Interrupt_Handler(struct Interrupt_State *state) {

	/* For now, just check if the receiver interrupt was signalled. */
	if(mmio_readbit(REG_UART_RAWINT, UART_RXIC)) {

		/* Keep de-queueing keys as long as data is available. */
		while ( !(mmio_read(UART0_FR) & (1 << 4)) ) { 
			Keyboard_Interrupt_Handler(state);
		}

		mmio_setbit(REG_UART_INT_CLEAR, UART_RXIC);
	}

}

void Init_UART() 
{
	// Disable UART0.
	mmio_write(UART0_CR, 0x00000000);
	// Setup the GPIO pin 14 && 15.

	// Disable pull up/down for all GPIO pins & delay for 150 cycles.
	mmio_write(GPPUD, 0x00000000);
	delay(150);

	// Disable pull up/down for pin 14,15 & delay for 150 cycles.
	mmio_write(GPPUDCLK0, (1 << 14) | (1 << 15));
	delay(150);

	// Write 0 to GPPUDCLK0 to make it take effect.
	mmio_write(GPPUDCLK0, 0x00000000);

	// Clear pending interrupts.
	mmio_write(UART0_ICR, 0x7FF);

	// Set integer & fractional part of baud rate.
	// Divider = UART_CLOCK/(16 * Baud)
	// Fraction part register = (Fractional part * 64) + 0.5
	// UART_CLOCK = 3000000; Baud = 115200.

	// Divider = 3000000 / (16 * 115200) = 1.627 = ~1.
	// Fractional part register = (.627 * 64) + 0.5 = 40.6 = ~40.
	mmio_write(UART0_IBRD, 1);
	mmio_write(UART0_FBRD, 40);

	// 8 bit data transmissio (1 stop bit, no parity).
	mmio_write(UART0_LCRH, (1 << 5) | (1 << 6));

	// Mask all interrupts.
	//mmio_write(UART0_IMSC, (1 << 1) | (1 << 4) | (1 << 5) | (1 << 6) |
	//		(1 << 7) | (1 << 8) | (1 << 9) | (1 << 10));
	mmio_write(UART0_IMSC, (1 << UART_RXIC));

	// Enable UART0, receive & transfer part of UART.
	mmio_write(UART0_CR, (1 << 0) | (1 << 8) | (1 << 9));

	UART_PrintString("Done initializing UART.\n\r");

	/* Install the interrupt handler for UART0 on PIC */
#ifdef RPI_BOOT
	Install_Shared_IRQ_Handler(UART0_IRQ, &UART_Interrupt_Handler);
#else
	Install_Primary_IRQ_Handler(UART0_IRQ, &UART_Interrupt_Handler);
#endif
}

void UART_Print_Char(int ch) {
	// Wait for UART to become ready to transmit.
	while ( mmio_read(UART0_FR) & (1 << 5) ) { }
	mmio_write(UART0_DR, ch);
}

unsigned char UART_Get_Char()
{
	// Wait for UART to have recieved something.
	while ( mmio_read(UART0_FR) & (1 << 4) ) { }
	return mmio_read(UART0_DR);
}

void UART_Write(const unsigned char* buffer, ulong_t size)
{
	ulong_t i;
	for ( i = 0; i < size; i++ )
		UART_Print_Char(buffer[i]);
}

void UART_PrintString(const char* str)
{
	UART_Write((const unsigned char*) str, strlen(str));
}
