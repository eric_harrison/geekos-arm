/*
 * Keyboard driver
 * Copyright (c) 2001,2004 David H. Hovemeyer <daveho@cs.umd.edu>
 * $Revision: 1.15 $
 * 
 * This is free software.  You are permitted to use,
 * redistribute, and modify it as specified in the file "COPYING".
 */

/*
 * Information sources:
 * - Chapter 8 of _The Undocumented PC_, 2nd ed, by Frank van Gilluwe,
 *   ISBN 0-201-47950-8.
 * - Pages 400-409 of _The Programmers PC Sourcebook_, by Thom Hogan,
 *   ISBN 1-55615-118-7.
 */

/*
 * Credits:
 * - Peter Gnodde <peter@pcswebdesign.nl> added support for
 *   the CTRL and ALT modifiers
 */

/* nspring added partial e0 (101-key, right control) stuff */

/*
 * TODO list:
 * - Right now we're assuming an 83-key keyboard.
 *   Should add support for 101+ keyboards.
 * - Should toggle keyboard LEDs.
 */

#include <geekos/kthread.h>
#include <geekos/kassert.h>
#include <geekos/screen.h>
#include <geekos/keyboard.h>
#include <geekos/uart.h>
#include <geekos/int.h>

#define DEBUG_KEYBOARD(x...)
// #define DEBUG_KEYBOARD(x...) Print("KBD: " x)

/* ----------------------------------------------------------------------
 * Private data and functions
 * ---------------------------------------------------------------------- */

/*
 * Queue for keycodes, in case they arrive faster than consumer
 * can deal with them.
 */
#define QUEUE_SIZE 256
#define QUEUE_MASK 0xff
#define NEXT(index) (((index) + 1) & QUEUE_MASK)
static Keycode s_queue[QUEUE_SIZE];
static int s_queueHead, s_queueTail;

/*
 * Wait queue for thread(s) waiting for keyboard events.
 */
static struct Thread_Queue s_waitQueue;

static __inline__ bool Is_Queue_Empty(void) {
	return s_queueHead == s_queueTail;
}

static __inline__ bool Is_Queue_Full(void) {
	return NEXT(s_queueTail) == s_queueHead;
}

static __inline__ void Enqueue_Keycode(Keycode keycode) {
	if(!Is_Queue_Full()) {
		s_queue[s_queueTail] = keycode;
		s_queueTail = NEXT(s_queueTail);
	}
}

static __inline__ Keycode Dequeue_Keycode(void) {
	Keycode result;
	KASSERT(!Is_Queue_Empty());
	result = s_queue[s_queueHead];
	s_queueHead = NEXT(s_queueHead);
	return result;
}

/* UART0 RXIM - Receive interrupt mask handler */
void Keyboard_Interrupt_Handler(struct Interrupt_State *state) {
	uchar_t status;
	Keycode keycode;

	/* Check status of UART */
	keycode = UART_Get_Char();

	/* Put the keycode in the buffer */
	Enqueue_Keycode(keycode);

	/* Wake up event consumers */
	Wake_Up(&s_waitQueue);

	/* Pick new thread to run */
	g_needReschedule[Get_CPU_ID()] = true;
}

/* ----------------------------------------------------------------------
 * Public functions
 * ---------------------------------------------------------------------- */

void Init_Keyboard(void) {

	Print("Initializing keyboard...\n");

	/* Buffer is initially empty. */
	s_queueHead = s_queueTail = 0;

}

/*
 * Poll for a key event.
 * Returns true if a key is available,
 * false if not.  If a key event is available,
 * it will be stored in the location pointed to
 * by keycode.
 */
bool Read_Key(Keycode * keycode) {

	bool result, iflag;

	iflag = Begin_Int_Atomic();

	result = !Is_Queue_Empty();
	if(result) {
		*keycode = Dequeue_Keycode();
	}

	End_Int_Atomic(iflag);

	return result;
}

/*
 * Wait for a keycode to arrive.
 * Uses the keyboard wait queue to sleep until
 * a keycode arrives.
 */
Keycode Wait_For_Key(void) {
	bool gotKey, iflag;
	Keycode keycode;

	iflag = Begin_Int_Atomic();

	do {
		gotKey = !Is_Queue_Empty();
		if(gotKey)
			keycode = Dequeue_Keycode();
		else {
			Wait(&s_waitQueue);
		}
	}
	while (!gotKey);

	End_Int_Atomic(iflag);

	return keycode;
}
