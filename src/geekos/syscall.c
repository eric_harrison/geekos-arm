/*
 * System call handlers
 * Copyright (c) 2001,2003,2004 David H. Hovemeyer <daveho@cs.umd.edu>
 * Copyright (c) 2003,2013,2014 Jeffrey K. Hollingsworth <hollings@cs.umd.edu>
 *
 * All rights reserved.
 *
 * This code may not be resdistributed without the permission of the copyright holders.
 * Any student solutions using any of this code base constitute derviced work and may
 * not be redistributed in any form.  This includes (but is not limited to) posting on
 * public forums or web sites, providing copies to (past, present, or future) students
 * enrolled in similar operating systems courses the University of Maryland's CMSC412 course.
 *
 */

#include <geekos/syscall.h>
#include <geekos/errno.h>
#include <geekos/kthread.h>
#include <geekos/int.h>
#include <geekos/elf.h>
#include <geekos/malloc.h>
#include <geekos/screen.h>
#include <geekos/keyboard.h>
#include <geekos/string.h>
#include <geekos/user.h>
#include <geekos/timer.h>
#include <geekos/vfs.h>
#include <geekos/signal.h>
#include <geekos/sem.h>
#include <geekos/projects.h>
#include <geekos/uart.h>
#include <geekos/serial.h>

#include <geekos/sys_net.h>
#include <geekos/pipe.h>
#include <geekos/mem.h>
#include <geekos/smp.h>

extern Spin_Lock_t kthreadLock;

/*
 * Allocate a buffer for a user string, and
 * copy it into kernel space.
 * Interrupts must be disabled.
 */
/* "extern" to note that it's used by semaphore and networking system calls, defined 
   in another file */
/* need not be called with interrupts disabled, since it should move data from 
   user space (which is blocked in the kernel) into a newly-allocated buffer */
extern int Copy_User_String(ulong_t uaddr, ulong_t len, ulong_t maxLen,
                            char **pStr) {
    int rc = 0;
    char *str;


    /* Ensure that string isn't too long. */
    if(len > maxLen)
        return EINVALID;

    /* Allocate space for the string. */
    str = (char *)Malloc(len + 1);
    if(str == 0) {
        rc = ENOMEM;
        goto done;
    }

    /* Copy data from user space. */
    if(!Copy_From_User(str, uaddr, len)) {
        rc = EINVALID;
        Free(str);
        goto done;
    }
    str[len] = '\0';

    /* Success! */
    *pStr = str;

  done:
    return rc;
}

/*
 * Null system call.
 * Does nothing except immediately return control back
 * to the interrupted user program.
 * Params:
 *  state - processor registers from user mode
 *
 * Returns:
 *   always returns the value 0 (zero)
 */
static int Sys_Null(struct Interrupt_State *state
                    __attribute__ ((unused))) {
    return 0;
}

/*
 * Exit system call.
 * The interrupted user process is terminated.
 * Params:
 *   state->r0 - process exit code
 * Returns:
 *   Never returns to user mode!
 */
static int Sys_Exit(struct Interrupt_State *state) {
    Enable_Interrupts();        /* ns14 */
    Exit(state->r0);
    /* We will never get here. */
}

/*
** Shutdown Computer
** Normaly not within a user's powers,
** but it helps automate testing
*/
extern void Hardware_Shutdown();        /* is in keyboard.c for odd reasons */
static int Sys_ShutDown(struct Interrupt_State *state) {
    Print("------------------- THE END ------------------\n");
    Hardware_Shutdown();
    /* We will never get here. */
    return 0;
}

static Spin_Lock_t sprintLock;

/* Get the current cursor position.
 * Not supported in Raspberry Pi headless mode.
 * Returns: 0 if successful, -1 otherwise
 */
static int Sys_GetCursor(struct Interrupt_State *state) {
	return -1;
}

/*
 * Set the current cursor position.
 * Not supported in Raspberry Pi headless mode.
 */
static int Sys_PutCursor(struct Interrupt_State *state) {
	return -1;
}

/*
 * Print a string to the console.
 * Params:
 *   state->r0 - user pointer of string to be printed
 *   state->r1 - number of characters to print
 * Returns: 0 if successful, -1 if not
 */
static int Sys_PrintString(struct Interrupt_State *state) {
		int rc;
    uint_t length = state->r1;
    unsigned char *buf = 0;

    //    unlockKernel();
    Enable_Interrupts();

    if(length > 0) {
        /* Copy string into kernel. */
        if((rc =
            Copy_User_String(state->r0, length, 1023,
                             (char **)&buf)) != 0)
            goto done;

        /* in reality, one wouldn't abort on this sort of thing.  but we do that
           just in case it helps track down a bug that much sooner */
        /* length is greater than zero, so someone thought there was a string here,
           but the first character is null, so they were either wrong or something 
           poor occurred. */
        if(!buf[0]) {
            Dump_Interrupt_State(state);
            KASSERT0(buf[0],
                     "Attempted to print a null string; this is likely a memory error.");
        }

				/* Write to console; only one may write. */
				Spin_Lock(&sprintLock);

				bool iflag = Begin_Int_Atomic();

				if(buf[0] == 127)
					buf[0] = 0x08;
				PrintString(buf, length);

				End_Int_Atomic(iflag);

        Spin_Unlock(&sprintLock);

    }

  done:
    if(buf != 0)
        Free(buf);

    /* somehow, ends up being locked here */
    // lockKernel();
    Disable_Interrupts();

    return rc;
}

/*
 * Get a single key press from the console.
 * Suspends the user process until a key press is available.
 * Params:
 *   state - processor registers from user mode
 * Returns: the key code
 *          -1 if this is a background process
 */
static int Sys_GetKey(struct Interrupt_State *state) {
	return Serial_Get_Key();
}

/*
 * Set the current text attributes.
 * Params:
 *   state->r0 - character attributes to use
 * Returns: always returns 0
 */
static int Sys_SetAttr(struct Interrupt_State *state) {
    return 0;
}

/*
 * Create a new user process.
 * Params:
 *   state->r0 - user address of name of executable
 *   state->r1 - length of executable name
 *   state->r2 - user address of command string
 *   state->r3 - length
 *   detached - stored at state->r11 + 4 + 0 (first arg in x86 format)
 * Returns: pid of process if successful, error code (< 0) otherwise
 */
static int Sys_Spawn(struct Interrupt_State *state) {

    int rc;
    char *program = 0;
    char *command = 0;
    struct Kernel_Thread *process = NULL;

    Enable_Interrupts();

		/* Load in extra arguments */
		int detached;

		Copy_From_User(&detached, state->r11 + 4 + 0*sizeof(ulong_t), sizeof(ulong_t));

    /* Copy program name and command from user space. */
    if((rc =
        Copy_User_String(state->r0, state->r1, VFS_MAX_PATH_LEN,
                         &program)) != 0 ||
       (rc =
        Copy_User_String(state->r2, state->r3, 1023, &command)) != 0) {
			Print("Spawn copy error: rc is %d\n\r", rc);
			goto done;
		}

    /*
     * Now that we have collected the program name and command string
     * from user space, we can try to actually spawn the process.
     */
    rc = Spawn(program, command, &process, detached);

    if(rc == 0) {
        KASSERT(process != 0);
        rc = process->pid;
    }

  done:
    if(program != 0)
        Free(program);
    if(command != 0)
        Free(command);

    Disable_Interrupts();

    return rc;
}

/*
 * Wait for a process to exit.
 * Params:
 *   state->r0 - pid of process to wait for
 * Returns: the exit code of the process,
 *   or error code (< 0) on error
 */
static int Sys_Wait(struct Interrupt_State *state) {
    int exitCode;
    struct Kernel_Thread *kthread = Lookup_Thread(state->r0, 0);
    if(kthread == 0) {
        // can't find the process id passed
        return EINVALID;
    }

    if(kthread->detached) {
        // can't wait on a detached process
        return EINVALID;
    }
    Enable_Interrupts();
    exitCode = Join(kthread);
    Disable_Interrupts();

    return exitCode;
}

/*
 * Get pid (process id) of current thread.
 * Params:
 *   state - processor registers from user mode
 * Returns: the pid of the current thread
 */
static int Sys_GetPID(struct Interrupt_State *state) {
    return CURRENT_THREAD->pid;
}


extern struct All_Thread_List s_allThreadList;
extern struct Thread_Queue s_runQueue;


/*
 * Get information about the running processes
 * Params:
 *   state->r0 - pointer to user memory containing an array of
 *   Process_Info structs
 *   state->r1 - length of the passed in array in memory
 * Returns: -1 on failure
 *          0 if size of user memory too small
 *          N the number of entries in the table, on success
 */
static int Sys_PS(struct Interrupt_State *state) {
    TODO_P(PROJECT_BACKGROUND_JOBS, "Sys_PS system call");
    return 0;
}


/*
 * Send a signal to a process
 * Params:
 *   state->r0 - pid of process to send signal to
 *   state->r1 - signal number
 * Returns: 0 on success or error code (< 0) on error
 */
static int Sys_Kill(struct Interrupt_State *state) {
    TODO_P(PROJECT_SIGNALS, "Sys_Kill system call");
    return 0;
}

/*
 * Register a signal handler for a process
 * Params:
 *   state->r0 - pointer to handler function
 *   state->r1 - signal number
 * Returns: 0 on success or error code (< 0) on error
 */
static int Sys_Signal(struct Interrupt_State *state) {
    TODO_P(PROJECT_SIGNALS, "Sys_Signal system call");
    return 0;
}

/*
 * Register the Return_Signal trampoline for this process.
 * Signals cannot be delivered until this is registered.
 * Params:
 *   state->r0 - pointer to Return_Signal function
 * may not be used:
 *   state->r1 - pointer to the default handler
 *   state->r2 - pointer to the ignore handler
 *
 * Returns: 0 on success or error code (< 0) on error
 */
static int Sys_RegDeliver(struct Interrupt_State *state) {
    return 0;
    TODO_P(PROJECT_SIGNALS, "Sys_RegDeliver system call");
}

/*
 * Complete signal handling for this process.
 * Params:
 *   none
 *
 * Returns: not expected to "return"
 */
static int Sys_ReturnSignal(struct Interrupt_State *state) {
    TODO_P(PROJECT_SIGNALS, "Sys_ReturnSignal system call");
    return EUNSUPPORTED;
}

/*
 * Reap a child process that has died
 * Params:
 *   state->r0 - pointer to status of process reaped
 * Returns: pid of reaped process on success, -1 on error.
 */
static int Sys_WaitNoPID(struct Interrupt_State *state) {
    TODO_P(PROJECT_SIGNALS, "Sys_WaitNoPID system call");
    return EUNSUPPORTED;
}

/*
 * Set the scheduling policy.
 * Params:
 *   state->r0 - policy,
 *   state->r1 - number of ticks in quantum
 * Returns: 0 if successful, -1 otherwise
 */
static int Sys_SetSchedulingPolicy(struct Interrupt_State *state) {
    TODO_P(PROJECT_SCHEDULING, "SetSchedulingPolicy system call");
    return 0;
}

/*
 * Get the time of day.
 * Params:
 *   state - processor registers from user mode
 *
 * Returns: value of the g_numTicks global variable
 */
static int Sys_GetTimeOfDay(struct Interrupt_State *state
                            __attribute__ ((unused))) {
    return g_numTicks;
}

/*
 * Mount a filesystem.
 * Params:
 * state->r0 - contains a pointer to the Mount_Syscall_Args structure
 *   which contains the block device name, mount prefix,
 *   and filesystem type
 *
 * Returns:
 *   0 if successful, error code if unsuccessful
 */
static int Sys_Mount(struct Interrupt_State *state) {
    int rc = 0;
    struct VFS_Mount_Request *args = 0;


    /* Allocate space for VFS_Mount_Request struct. */
    if((args =
        (struct VFS_Mount_Request *)
        Malloc(sizeof(struct VFS_Mount_Request))) == 0) {
        rc = ENOMEM;
        goto done;
    }

    /* Copy the mount arguments structure from user space. */
    if(!Copy_From_User
       (args, state->r0, sizeof(struct VFS_Mount_Request))) {
        rc = EINVALID;
        goto done;
    }

    /*
     * Hint: use devname, prefix, and fstype from the args structure
     * and invoke the Mount() VFS function.  You will need to check
     * to make sure they are correctly nul-terminated.
     */
    TODO_P(PROJECT_FS, "Mount system call");
    rc = EUNSUPPORTED;
  done:
    if(args != 0)
        Free(args);
    return rc;
}

static int get_path_from_registers(uint_t addr, uint_t length,
                                   char **pPath) {
    if(length > 1024) {
        return ENAMETOOLONG;
    }
    *pPath = Malloc(length + 1);
    if(!*pPath) {
        return ENOMEM;
    }
    if(!Copy_From_User(*pPath, addr, length)) {
        Free(*pPath);
        return EINVALID;
    }
    (*pPath)[length] = '\0';
    return 0;
}

static int next_descriptor() {
    int descriptor;
    for(descriptor = 0;
        descriptor < USER_MAX_FILES &&
        CURRENT_THREAD->userContext->file_descriptor_table[descriptor] !=
        0; descriptor++) ;
    if(descriptor == USER_MAX_FILES) {
        return EMFILE;
    }
    return descriptor;
}

static int add_file_to_descriptor_table(struct File *file) {
    int descriptor = next_descriptor();
    if(descriptor >= 0) {
        CURRENT_THREAD->userContext->file_descriptor_table[descriptor] =
            file;
    }
    return descriptor;
}

/*
 * Open a file.
 * Params:
 *   state->r0 - address of user string containing path of file to open
 *   state->r1 - length of path
 *   state->r2 - file mode flags
 *
 * Returns: a file descriptor (>= 0) if successful,
 *   or an error code (< 0) if unsuccessful
 */
static int Sys_Open(struct Interrupt_State *state) {
    char *path;
    struct File *file;
    int rc = 0;

    Enable_Interrupts();

    rc = get_path_from_registers(state->r0, state->r1, &path);
    if(rc != 0) {
        goto leave;
    }

    rc = next_descriptor();
    if(rc < 0) {
        Free(path);
        goto leave;
    }

    rc = Open(path, state->r2, &file);
    Free(path);

  leave:
    Disable_Interrupts();
    if(rc >= 0) {
        return add_file_to_descriptor_table(file);
    } else {
        return rc;
    }
}

/*
 * Open a directory.
 * Params:
 *   state->r0 - address of user string containing path of directory to open
 *   state->r1 - length of path
 *
 * Returns: a file descriptor (>= 0) if successful,
 *   or an error code (< 0) if unsuccessful
 */
static int Sys_OpenDirectory(struct Interrupt_State *state) {
    TODO_P(PROJECT_FS, "Open directory system call");
    return EUNSUPPORTED;
}

/*
 * Close an open file or directory.
 * Params:
 *   state->r0 - file descriptor of the open file or directory
 * Returns: 0 if successful, or an error code (< 0) if unsuccessful
 */
static int Sys_Close(struct Interrupt_State *state) {
    /* where is the file table? */
    if(state->r0 > USER_MAX_FILES) {
        Print("unable to close fd index %d, out of range.\n", state->r0);
        return EINVALID;
    }
    if(CURRENT_THREAD->userContext->file_descriptor_table[state->r0]) {
        Enable_Interrupts();
        Close(CURRENT_THREAD->userContext->
              file_descriptor_table[state->r0]);
        Disable_Interrupts();
        CURRENT_THREAD->userContext->file_descriptor_table[state->r0] =
            0;
        return 0;
    } else {
        // Print("unable to close fd index %d, nothing there.\n", state->r0);
        return ENOTFOUND;
    }
}

/*
 * Delete a file.
 * Params:
 *   state->r0 - address of user string containing path to delete
 *   state->r1 - length of path
 *   state->r2 - recursive flags
 *
 * Returns: 0 if successful, error code (< 0) if unsuccessful
 */
static int Sys_Delete(struct Interrupt_State *state) {
    TODO_P(PROJECT_FS, "Delete system call");
    return EUNSUPPORTED;
}

/*
 * Rename a file.
 * Params:
 *   state->r0 - address of user string containing old path 
 *   state->r1 - length of old path
 *   state->r2 - address of user string containing new path 
 *   state->r3 - length of new path
 *
 * Returns: 0 if successful, error code (< 0) if unsuccessful
 */
static int Sys_Rename(struct Interrupt_State *state) {
    TODO_P(PROJECT_FS, "Rename system call");
    return EUNSUPPORTED;
}

/*
 * Link a file.
 * Params:
 *   state->r0 - address of user string containing old path 
 *   state->r1 - length of old path
 *   state->r2 - address of user string containing new path 
 *   state->r3 - length of new path
 *
 * Returns: 0 if successful, error code (< 0) if unsuccessful
 */
static int Sys_Link(struct Interrupt_State *state) {
    TODO_P(PROJECT_FS, "Link system call");
    return EUNSUPPORTED;
}

/*
 * Link a file.
 * Params:
 *   state->r0 - address of user string containing old path 
 *   state->r1 - length of old path
 *   state->r2 - address of user string containing new path 
 *   state->r3 - length of new path
 *
 * Returns: 0 if successful, error code (< 0) if unsuccessful
 */
static int Sys_SymLink(struct Interrupt_State *state) {
    TODO_P(PROJECT_FS, "Link system call");
    return EUNSUPPORTED;
}

/*
 * Read from an open file.
 * Params:
 *   state->r0 - file descriptor to read from
 *   state->r1 - user address of buffer to read into
 *   state->r2 - number of bytes to read
 *
 * Returns: number of bytes read, 0 if end of file,
 *   or error code (< 0) on error
 */
static int Sys_Read(struct Interrupt_State *state) {
    int bytes_read = 0;
    /* where is the file table? */
    if(state->r0 > USER_MAX_FILES) {
        return EINVALID;
    }
    if(CURRENT_THREAD->userContext->file_descriptor_table[state->r0]) {
        void *data_buffer;
        Enable_Interrupts();
        data_buffer = Malloc(state->r2);
        if(!data_buffer) {
            return ENOMEM;
        }
        bytes_read =
            Read(CURRENT_THREAD->userContext->
                 file_descriptor_table[state->r0], data_buffer,
                 state->r2);
        if(bytes_read > 0) {
            if(!Copy_To_User(state->r1, data_buffer, bytes_read)) {
                bytes_read = EINVALID;
            }
        }
        Free(data_buffer);
        Disable_Interrupts();
        return bytes_read;
    } else {
        return ENOTFOUND;
    }
}

/*
 * Read a directory entry from an open directory handle.
 * Params:
 *   state->r0 - file descriptor of the directory
 *   state->r1 - user address of struct VFS_Dir_Entry to copy entry into
 * Returns: 0 if successful, error code (< 0) if unsuccessful
 */
static int Sys_ReadEntry(struct Interrupt_State *state) {
    TODO_P(PROJECT_FS, "ReadEntry system call");
    return EUNSUPPORTED;
}

/*
 * Write to an open file.
 * Params:
 *   state->r0 - file descriptor to write to
 *   state->r1 - user address of buffer get data to write from
 *   state->r2 - number of bytes to write
 *
 * Returns: number of bytes written,
 *   or error code (< 0) on error
 */
static int Sys_Write(struct Interrupt_State *state) {
    int bytes_written = 0;
    /* where is the file table? */
    if(state->r0 > USER_MAX_FILES) {
        return EINVALID;
    }
    if(CURRENT_THREAD->userContext->file_descriptor_table[state->r0]) {
        Enable_Interrupts();
        void *data_buffer = Malloc(state->r2);
        if(!data_buffer) {
            Disable_Interrupts();
            return ENOMEM;
        }
        if(!Copy_From_User(data_buffer, state->r1, state->r2)) {
            Free(data_buffer);
            Disable_Interrupts();
            return EINVALID;
        }
        bytes_written =
            Write(CURRENT_THREAD->userContext->
                  file_descriptor_table[state->r0], data_buffer,
                  state->r2);
        Free(data_buffer);
        Disable_Interrupts();
        return bytes_written;
    } else {
        return ENOTFOUND;
    }
}

/*
 * Get file metadata.
 * Params:
 *   state->r0 - address of user string containing path of file
 *   state->r1 - length of path
 *   state->r2 - user address of struct VFS_File_Stat object to store metadata in
 *
 * Returns: 0 if successful, error code (< 0) if unsuccessful
 */
static int Sys_Stat(struct Interrupt_State *state) {
    TODO_P(PROJECT_FS, "Stat system call");
    return EUNSUPPORTED;
}

/*
 * Get metadata of an open file.
 * Params:
 *   state->r0 - file descriptor to get metadata for
 *   state->r1 - user address of struct VFS_File_Stat object to store metadata in
 *
 * Returns: 0 if successful, error code (< 0) if unsuccessful
 */
static int Sys_FStat(struct Interrupt_State *state) {
    TODO_P(PROJECT_FS, "FStat system call");
    return EUNSUPPORTED;
}

/*
 * Change the access position in a file
 * Params:
 *   state->r0 - file descriptor
 *   state->r1 - position in file
 *
 * Returns: 0 if successful, error code (< 0) if unsuccessful
 */
static int Sys_Seek(struct Interrupt_State *state) {
    TODO_P(PROJECT_FS, "Seek system call");
    return EUNSUPPORTED;
}

/*
 * Create directory
 * Params:
 *   state->r0 - address of user string containing path of new directory
 *   state->r1 - length of path
 *
 * Returns: 0 if successful, error code (< 0) if unsuccessful
 */
static int Sys_CreateDir(struct Interrupt_State *state) {
    TODO_P(PROJECT_FS, "CreateDir system call");
    return EUNSUPPORTED;
}

/*
 * Flush filesystem buffers
 * Params: none
 * Returns: 0 if successful, error code (< 0) if unsuccessful
 */
static int Sys_Sync(struct Interrupt_State *state) {
    TODO_P(PROJECT_FS, "Sync system call");
    return EUNSUPPORTED;
}

/*
 * Format a device
 * Params:
 *   state->r0 - address of user string containing device to format
 *   state->r1 - length of device name string
 *   state->r2 - address of user string containing filesystem type 
 *   state->r3 -  length of filesystem type string
 * Returns: 0 if successful, error code (< 0) if unsuccessful
 */
static int Sys_Format(struct Interrupt_State *state) {
    int rc = 0;
    char *devname = 0, *fstype = 0;

    Enable_Interrupts();

    if((rc =
        Copy_User_String(state->r0, state->r1, BLOCKDEV_MAX_NAME_LEN,
                         &devname)) != 0 ||
       (rc =
        Copy_User_String(state->r2, state->r3, VFS_MAX_FS_NAME_LEN,
                         &fstype)) != 0)
        goto done;

    rc = Format(devname, fstype);

  done:
    if(devname != 0)
        Free(devname);
    if(fstype != 0)
        Free(fstype);
    Disable_Interrupts();
    return rc;
}

/*
 * Read a block from a device
 * Params:
 *   state->r0 - address of user string containing block device name
 *   state->r1 - length of block device name string
 *   state->r2 - address of user buffer to read into
 *   state->r3 - length to read into user buffer
 *   blockNum - stored at state->r11 + 4 + 0 (block # to read from)

 * Returns: 0 if successful, error code (< 0) if unsuccessful
 */
static int Sys_ReadBlock(struct Interrupt_State *state) {
    TODO_P(PROJECT_FS, "ReadBlock system call");
    return EUNSUPPORTED;
}

/*
 * Write a block to a device
 * Params:
 *   state->r0 - address of user string containing device name
 *   state->r1 - length of block device name string
 *   state->r2 - address of user buffer to read into
 *   state->r3 - length to read into user buffer
 *   blockNum - stored at state->r11 + 4 + 0 (block # to read from)

 * Returns: 0 if successful, error code (< 0) if unsuccessful
 */
static int Sys_WriteBlock(struct Interrupt_State *state) {
    TODO_P(PROJECT_FS, "WriteBlock system call");
    return EUNSUPPORTED;
}


static int Sys_GetUid(struct Interrupt_State *state) {
    TODO_P(PROJECT_USER, "Sys_GetUid system call");
    return EUNSUPPORTED;
}

static int Sys_SetSetUid(struct Interrupt_State *state) {
    TODO_P(PROJECT_USER, "Sys_SetSetUid system call");
    return EUNSUPPORTED;
}

static int Sys_SetEffectiveUid(struct Interrupt_State *state) {
    TODO_P(PROJECT_USER, "Sys_SetEffectiveUid system call");
    return EUNSUPPORTED;
}

static int Sys_SetAcl(struct Interrupt_State *state) {
    TODO_P(PROJECT_USER, "Sys_SetAcl system call");
    return EUNSUPPORTED;
}

extern void SB16_Play_File(const char *filename);
static int Sys_PlaySoundFile(struct Interrupt_State *state) {
    TODO_P(PROJECT_SOUND, "PlaySoundFile system call");
    return 0;
}

/* 
 * Create a pipe.
 * Params:
 *   state->r0 - address of file descriptor for the read side
 *   state->r1 - address of file descriptor for the write side
 */
static int Sys_Pipe(struct Interrupt_State *state) {
    TODO_P(PROJECT_PIPE, "Pipe system call");
    return EUNSUPPORTED;
}



static int Sys_Fork(struct Interrupt_State *state) {
    TODO_P(PROJECT_FORK, "Fork system call");
    return EUNSUPPORTED;
}

/* 
 * Exec a new program in this process.
 * Params:
 *   state->r0 - user address of name of executable
 *   state->r1 - length of executable name
 *	 state->r2 - user address of command string
 *   state->r3 - length of command string
 * Returns: doesn't if successful, error code (< 0) otherwise
 */
static int Sys_Execl(struct Interrupt_State *state) {
    TODO_P(PROJECT_FORK, "Execl system call");
    return EUNSUPPORTED;
}

/* 
 * The following is a crude trigger for dumping kernel 
 * statistics to the console output.  It is not generally
 * how one should output kernel statistics to user space.
 */

static int Sys_Diagnostic(struct Interrupt_State *state) {
    (void)state;                /* warning appeasement */
    Enable_Interrupts();
    Dump_Blockdev_Stats();
    Disable_Interrupts();
    return 0;
}

/* 
 * Retrieve disk properties
 * Params:
 *   state->r0 - path name to mounted file system
 *   state->r1 - length of path name
 *   state->r2 - user address for block size
 *   state->r3 - user address for number of blocks on disk
 * Returns: doesn't if successful, error code (< 0) otherwise
 */
static int Sys_Disk_Properties(struct Interrupt_State *state) {
    char *path;
    unsigned int block_size, blocks_per_disk;
    int rc;
    Enable_Interrupts();

    Copy_User_String(state->r0, state->r1, 100, &path);
    rc = Disk_Properties(path, &block_size, &blocks_per_disk);
    if(rc == 0) {
        Copy_To_User(state->r2, &block_size, sizeof(unsigned int));
        Copy_To_User(state->r3, &blocks_per_disk, sizeof(unsigned int));
    }
    Disable_Interrupts();
    return 0;
}

/* 
 * Set Resource Limits
 * Params:
 *   state->r0 - resource to limit
 *   state->r1 - limit
 * Returns: doesn't if successful, error code (< 0) otherwise
 */
static int Sys_Limit(struct Interrupt_State *state) {
    TODO_P(PROJECT_LIMIT, "Limit system call");
    return EUNSUPPORTED;
}

static int Sys_Set_Affinity(struct Interrupt_State *state) {
    return EUNSUPPORTED;
}

static int Sys_Get_Affinity(struct Interrupt_State *state) {
    return EUNSUPPORTED;
}

/*
 * Sys_Clone - create a new LWP, shares text and heap with parent
 *
 * Params:
 *   state->r0 - address of thread function to run 
 *   state->r1 - address of top of child's stack 
 * Returns: pid for child on sucess or EINVALID for error
 */
static int Sys_Clone(struct Interrupt_State *state) {
    TODO_P(PROJECT_CLONE, "Clone system call");
    return EUNSUPPORTED;
}

static int Sys_Mmap(struct Interrupt_State *state) {
    TODO_P(PROJECT_MMAP, "Mmap system call");
    return EUNSUPPORTED;
}

static int Sys_Munmap(struct Interrupt_State *state) {
    extern int Munmap_Impl(void *);

    TODO_P(PROJECT_MMAP, "Munmap system call");
    return EUNSUPPORTED;
}

static int Sys_Alarm(struct Interrupt_State *state) {
    TODO_P(PROJECT_SIGNALS, "Alarm");
    return EUNSUPPORTED;
}

static int Sys_Sbrk(struct Interrupt_State *state) {
    TODO_P(PROJECT_MALLOC,
           "underlying system call that allows malloc to work");
    return EUNSUPPORTED;
}

/*
 * Global table of system call handler functions.
 */
const Syscall g_syscallTable[] = {
    Sys_Null,
    Sys_Exit,
    Sys_PrintString,
    Sys_GetKey,
    Sys_SetAttr,
    Sys_GetCursor,
    Sys_PutCursor,
    Sys_Spawn,
    Sys_Wait,
    Sys_GetPID,
    Sys_Kill,
    Sys_PS,
    Sys_Signal,
    Sys_RegDeliver,
    Sys_ReturnSignal,
    Sys_WaitNoPID,
    /* Scheduling and semaphore system calls. */
    Sys_SetSchedulingPolicy,
    Sys_GetTimeOfDay,
    Sys_Open_Semaphore,
    Sys_P,
    Sys_V,
    Sys_Close_Semaphore,
    /* File I/O system calls. */
    Sys_Mount,
    Sys_Open,
    Sys_OpenDirectory,
    Sys_Close,
    Sys_Delete,
    Sys_Read,
    Sys_ReadEntry,
    Sys_Write,
    Sys_Stat,
    Sys_FStat,
    Sys_Seek,
    Sys_CreateDir,
    Sys_Sync,
    Sys_Format,
    Sys_ShutDown,
    Sys_ReadBlock,
    Sys_WriteBlock,
    /* Networking calls */
    Sys_EthPacketSend,
    Sys_EthPacketReceive,
    Sys_Arp,
    Sys_RouteAdd,
    Sys_RouteDel,
    Sys_RouteGet,
    Sys_IPConfigure,
    Sys_IPGet,
    Sys_IPSend,
    /* Socket API */
    Sys_Socket,
    Sys_Bind,
    Sys_Listen,
    Sys_Accept,
    Sys_Connect,
    Sys_Send,
    Sys_Receive,
    Sys_SendTo,
    Sys_ReceiveFrom,
    Sys_CloseSocket,
    /* User related calls */
    Sys_Limit,
    Sys_GetUid,
    Sys_SetSetUid,
    Sys_SetEffectiveUid,
    Sys_SetAcl,
    /* sound */
    Sys_PlaySoundFile,
    /* unix interface */
    Sys_Pipe,
    Sys_Fork,
    Sys_Execl,
    /* diagnostics and debugging */
    Sys_Diagnostic,
    Sys_Disk_Properties,
    /* SMP functions */
    Sys_Set_Affinity,
    Sys_Get_Affinity,
    Sys_Clone,
    /* memory mapped files */
    Sys_Mmap,
    Sys_Munmap,
    Sys_Alarm,
    Sys_Rename,
    Sys_Link,
    Sys_SymLink,
    Sys_Sbrk
};

/*
 * Number of system calls implemented.
 */
const unsigned int g_numSyscalls =
    sizeof(g_syscallTable) / sizeof(Syscall);
