/*
 * ATA (aka IDE) driver.
 * Copyright (c) 2001,2003,2004 David H. Hovemeyer <daveho@cs.umd.edu>
 * Copyright (c) 2003,2013,2014 Jeffrey K. Hollingsworth <hollings@cs.umd.edu>
 *
 * All rights reserved.
 *
 * This code may not be resdistributed without the permission of the copyright holders.
 * Any student solutions using any of this code base constitute derviced work and may
 * not be redistributed in any form.  This includes (but is not limited to) posting on
 * public forums or web sites, providing copies to (past, present, or future) students
 * enrolled in similar operating systems courses the University of Maryland's CMSC412 course.
 */

/*
 * Pseudo-block device used in conjunction with loading file system as part of kernel image.
 */

#include <geekos/ktypes.h>
#include <geekos/kassert.h>
#include <geekos/errno.h>
#include <geekos/malloc.h>
#include <geekos/string.h>
#include <geekos/int.h>
#include <geekos/timer.h>
#include <geekos/kthread.h>
#include <geekos/blockdev.h>
#include <geekos/pseudoblock.h>

#define BLOCK_SIZE 512

struct Thread_Queue s_fakeBlockWaitQueue;
struct Block_Request_List s_fakeBlockRequestQueue;

/* Symbols defined in linker used for block start */
extern char __pfat_start;
static char * pfat_start = &__pfat_start;

/*
 * Read a block at the logical block number indicated.
 */
static int Pseudo_Block_Read(int driveNum, int blockNum, char *buffer) {
	memcpy(buffer, pfat_start + blockNum * BLOCK_SIZE, BLOCK_SIZE);
	return 0;
}

/*
 * Write a block at the logical block number indicated.
 */
static int Pseudo_Block_Write(int driveNum, int blockNum, char *buffer) {
	memcpy(pfat_start + blockNum * BLOCK_SIZE, buffer, BLOCK_SIZE);
	return 0;
}

static int Pseudo_Block_Open(struct Block_Device *dev) {
    return 0;
}

static int Pseudo_Block_Close(struct Block_Device *dev) {
    return 0;
}

static int Pseudo_Block_Get_Num_Blocks(struct Block_Device *dev) {
	return 0;
}

static struct Block_Device_Ops s_fakeBlockDeviceOps = {
    Pseudo_Block_Open,
    Pseudo_Block_Close,
    Pseudo_Block_Get_Num_Blocks,
};

static void Pseudo_Block_Request_Thread(ulong_t arg __attribute__ ((unused))) {
    for(;;) {
        struct Block_Request *request;
        int rc;

        /* Wait for a request to arrive */
        request = Dequeue_Request(&s_fakeBlockRequestQueue, &s_fakeBlockWaitQueue);

        /* Do the I/O */
        if(request->type == BLOCK_READ)
            rc = Pseudo_Block_Read(request->dev->unit, request->blockNum,
                          request->buf);
        else
            rc = Pseudo_Block_Write(request->dev->unit, request->blockNum,
                           request->buf);

        /* Notify requesting thread of final status */
        Notify_Request_Completion(request, rc == 0 ? COMPLETED : ERROR,
                                  rc);
    }
}

void Init_Pseudo_Block_Device(const char * name) {
	/* Register pseudo block device */
	Register_Block_Device(name, &s_fakeBlockDeviceOps, 0, 0,
			&s_fakeBlockWaitQueue, &s_fakeBlockRequestQueue);
		  
	/* Start request thread */
	Start_Kernel_Thread(Pseudo_Block_Request_Thread, 0, PRIORITY_NORMAL, true,
			"{Pseudo-Block}");
}
