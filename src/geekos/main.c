/*
 * GeekOS C code entry point
 * Copyright (c) 2001,2003,2004 David H. Hovemeyer <daveho@cs.umd.edu>
 * Copyright (c) 2004, Iulian Neamtiu <neamtiu@cs.umd.edu>
 * Copyright (c) 2003,2013,2014 Jeffrey K. Hollingsworth <hollings@cs.umd.edu>
 *
 * All rights reserved.
 *
 * This code may not be resdistributed without the permission of the copyright holders.
 * Any student solutions using any of this code base constitute derviced work and may
 * not be redistributed in any form.  This includes (but is not limited to) posting on
 * public forums or web sites, providing copies to (past, present, or future) students
 * enrolled in similar operating systems courses the University of Maryland's CMSC412 course.
 *
 * $Revision: 1.53 $
 *
 */

#include <geekos/bootinfo.h>
#include <geekos/string.h>
#include <geekos/screen.h>
#include <geekos/mem.h>
#include <geekos/crc32.h>
#include <geekos/int.h>
#include <geekos/kthread.h>
#include <geekos/trap.h>
#include <geekos/timer.h>
#include <geekos/keyboard.h>
#include <geekos/pfat.h>
#include <geekos/vfs.h>
#include <geekos/user.h>
#include <geekos/paging.h>
#include <geekos/gosfs.h>
#include <geekos/gfs2.h>
#include <geekos/cfs.h>
#include <geekos/projects.h>
#include <geekos/sound.h>
#include <geekos/smp.h>
#include <geekos/serial.h>
#include <geekos/uart.h>
#include <geekos/syscall.h>
#include <geekos/pseudoblock.h>
#include <geekos/vga.h>

/*
 * Define this for a self-contained boot floppy
 * with a PFAT filesystem.  (Target "fd_aug.img" in
 * the makefile.)
 */
/*#define FD_BOOT*/

#define ROOT_DEVICE "emmc0"
#define ROOT_PREFIX "c"

#define INIT_PROGRAM "/" ROOT_PREFIX "/shell.exe"

static void Mount_Root_Filesystem(void);
static void Spawn_Init_Process(void);
static void Dummy_Shell(void);

/*
 * Kernel C code entry point.
 * Initializes kernel subsystems, mounts filesystems,
 * and spawns init process.
 */

extern int checkPaging(void);

void Hardware_Shutdown() {
	/* FIXME: It's not clear how to shutdown QEMU ARM. */
	while(1) ;
	KASSERT0(false,
			"Hardware_Shutdown() failed: QEMU likely run with incorrect options.\n\r");
}

void Main(ulong_t r0, ulong_t r1, ulong_t atags) {

	struct Boot_Info boot;
	boot.memSizeKB = 130048;

	Init_UART();

#ifdef VGA_OUTPUT
	Init_VGA();
#endif

	Init_Scheduler(0, (void *)KERNEL_STACK);

	Init_Interrupts(0);

	Init_Mem(&boot);

	Init_Startup_Threads(0);

	Init_Keyboard();

	Init_Timer();

	Init_VM(&boot);
	Init_CRC32();

	/* by modifying begin_int_atomic to autolock if not locked when interrupts are disabled, 
		 this lockKernel() became duplicative */
	/* lockKernel(); */

	TODO_P(PROJECT_VIRTUAL_MEMORY_A,
			"initialize virtual memory page tables.");
	Init_Traps();

	Init_Paging();

	//Init_SD_Card();
	Init_PFAT();
	Init_GFS2();
	Init_GOSFS();
	Init_CFS();

	/* Initialize Networking */
	/* 
		 Init_Network_Devices();
		 Init_ARP_Protocol();
		 Init_IP();
		 Init_Routing();
		 Init_Sockets();
		 Init_RIP();
		 */
	/* End networking subsystem init */

	/* Initialize Sound */
	Init_Sound_Devices();
	/* End sound init */

	/* Load user stack ptr - this is risky, but whatever. */
	//__asm__ __volatile__ ("mov sp, #0xff0000");
	//Dummy_Shell();

	Init_Pseudo_Block_Device(ROOT_DEVICE);
	Mount_Root_Filesystem();

	TODO_P(PROJECT_VIRTUAL_MEMORY_A, "initialize page file.");

	Print("Welcome to GeekOS!\n\r");

	TODO_P(PROJECT_SOUND, "play startup sound");

	Spawn_Init_Process();

	/* it's time to shutdown the system because Init exited. */
	Hardware_Shutdown();

	/* we should not get here */
}

static void Mount_Root_Filesystem(void) {
	if(Mount(ROOT_DEVICE, ROOT_PREFIX, "pfat") != 0)
		Print("Failed to mount /" ROOT_PREFIX " filesystem\n\r");
	else
		Print("Mounted /" ROOT_PREFIX " filesystem!\n\r");

}

static void Spawn_Init_Process(void) {
	int rc;
	struct Kernel_Thread *initProcess;

	/* Load and run a.exe, the "init" process */
	Print("Spawning init process (%s)\n\r", INIT_PROGRAM);
	rc = Spawn_Foreground(INIT_PROGRAM, INIT_PROGRAM, &initProcess);
	// Print("... spawned\n");

	if(rc != 0) {
		Print("Failed to spawn init process: error code = %d\n\r", rc);
	} else {
		/* Wait for it to exit */
		int exitCode = Join(initProcess);
		Print("Init process exited with code %d\n\r", exitCode);
	}
}

// nop for divide by zero errors
void raise() {

}
