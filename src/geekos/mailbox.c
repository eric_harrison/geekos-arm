/* 
 * Source Repository: https://github.com/brianwiddas/pi-baremetal
 *
 * Read and write from the RPI mailbox 
 * For more information, see
 * https://github.com/raspberrypi/firmware/wiki/Mailbox-property-interface
 */

#include <geekos/mailbox.h>
#include <geekos/mmio.h>

/* Mailbox memory addresses */
#define MAILBOX0READ 0x2000b880
#define MAILBOX0STATUS 0x2000b898
#define MAILBOX0WRITE 0x2000b8a0

/* Bit 31 set in status register if the write mailbox is full */
#define MAILBOX_FULL 0x80000000

/* Bit 30 set in status register if the read mailbox is empty */
#define MAILBOX_EMPTY 0x40000000

unsigned int readmailbox(unsigned int channel)
{
	unsigned int count = 0;
	unsigned int data;

	/* Loop until something is received from channel
	 * If nothing recieved, it eventually give up and returns 0xffffffff
	 */
	while(1)
	{
		while (mmio_read(MAILBOX0STATUS) & MAILBOX_EMPTY)
		{
			/* Need to check if this is the right thing to do */
			//flushcache();

			/* This is an arbritarily large number */
			if(count++ >(1<<25))
			{
				return 0xffffffff;
			}
		}

		/* 
		 * Read the data
		 */
		data = mmio_read(MAILBOX0READ);

		if ((data & 15) == channel)
			return data;
	}
}

void writemailbox(unsigned int channel, unsigned int data)
{
	/* Wait for mailbox to be not full */
	while (mmio_read(MAILBOX0STATUS) & MAILBOX_FULL)
	{
		/* Need to check if this is the right thing to do */
		//flushcache();
	}

	mmio_write(MAILBOX0WRITE, data | channel);
}
