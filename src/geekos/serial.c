#include <geekos/int.h>
#include <geekos/kthread.h>
#include <geekos/string.h>
#include <geekos/projects.h>
#include <geekos/uart.h>
#include <geekos/keyboard.h>
#include <geekos/fmtout.h>

#define KIN_BACKSPACE 0x7F
#define KIN_DELETE 0x08
#define KOUT_BACKSPACE 0x08
#define KOUT_DELETE 0x7F

void Init_Serial() {
    TODO_P(PROJECT_SERIAL, "Initialize a serial port driver");
}

int Serial_Get_Key() {
	int k = (int) Wait_For_Key();

	if(k == KIN_BACKSPACE) {
		k = KOUT_BACKSPACE;
	} else if(k == KIN_DELETE) {
		k = KOUT_DELETE;
	}

	return k;
}

/* no-op */
static void Serial_Print_Finish(struct Output_Sink *o __attribute__ ((unused))) {
	return;
}

static void Serial_Print_Emit(struct Output_Sink *o __attribute__ ((unused)), int ch) {
	UART_Print_Char(ch);
}

struct Output_Sink g_serialOutputSink = { &Serial_Print_Emit, &Serial_Print_Finish };

void Serial_Print_String(const unsigned char * buf, ulong_t len) {
	UART_Write(buf, len);
}
