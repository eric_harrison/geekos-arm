/*
 * GeekOS timer interrupt support
 * Copyright (c) 2001,2003,2004 David H. Hovemeyer <daveho@cs.umd.edu>
 * Copyright (c) 2003,2013,2014 Jeffrey K. Hollingsworth <hollings@cs.umd.edu>
 *
 * All rights reserved.
 *
 * This code may not be resdistributed without the permission of the copyright holders.
 * Any student solutions using any of this code base constitute derviced work and may
 * not be redistributed in any form.  This includes (but is not limited to) posting on
 * public forums or web sites, providing copies to (past, present, or future) students
 * enrolled in similar operating systems courses the University of Maryland's CMSC412 course.
 */

#include <geekos/kassert.h>
#include <geekos/projects.h>
#include <limits.h>
#include <geekos/mmio.h>
#include <geekos/int.h>
#include <geekos/idt.h>
#include <geekos/kthread.h>
#include <geekos/timer.h>
#include <geekos/smp.h>

#define MAX_TIMER_EVENTS 100

static int timerDebug = 0;
static int timeEventCount;
static int nextEventID;
static callbackTimerEvent pendingTimerEvents[MAX_TIMER_EVENTS];

/*
 * Global tick counter
 */
volatile ulong_t g_numTicks;

/*
 * Number of times the spin loop can execute during one timer tick; 
 * some callers would like to Micro_Delay before calibrating the timer
 * with interrupts. 
 */
static int s_spinCountPerTick = 10000;

/*
 * Number of ticks to wait before calibrating the delay loop.
 */
#define CALIBRATE_NUM_TICKS	3

/*
 * The default quantum; maximum number of ticks a thread can use before
 * we suspend it and choose another.
 */
#define DEFAULT_MAX_TICKS 4

/*
 * Settable quantum.
 */
unsigned int g_Quantum = DEFAULT_MAX_TICKS;

#undef DEBUG_TIMER
#ifdef DEBUG_TIMER
#  define Debug(args...) Print(args)
#else
#  define Debug(args...)
#endif

/* ----------------------------------------------------------------------
 * Private functions
 * ---------------------------------------------------------------------- */

void Timer_Interrupt_Handler(struct Interrupt_State *state) {
	int i;
	int id;
	struct Kernel_Thread *current = CURRENT_THREAD;

	//Print("TIMER INTERRUPT!\n\r");

	id = Get_CPU_ID();

	if(!id) {
		/* Update global number of ticks - only on core 0 so won't count a rate equal to number of cores */
		++g_numTicks;
	}


	/* Update per-thread number of ticks and per core ticks */
	++current->numTicks;
	++current->totalTime;
	CPUs[id].ticks++;

	/*
	 * If thread has been running for an entire quantum,
	 * inform the interrupt return code that we want
	 * to choose a new thread.
	 */
	if(!id) {
		// XXXX - only core #0 is currently handling new timer events
		/* update timer events */
		for(i = 0; i < timeEventCount; i++) {
			if(pendingTimerEvents[i].ticks == 0) {
				if(timerDebug)
					Print("timer: event %d expired (%d ticks)\n",
							pendingTimerEvents[i].id,
							pendingTimerEvents[i].origTicks);
				(pendingTimerEvents[i].callBack) (pendingTimerEvents[i].
						id);
			} else {
				if(timerDebug)
					Print("timer: event %d at %d ticks\n",
							pendingTimerEvents[i].id,
							pendingTimerEvents[i].ticks);
				pendingTimerEvents[i].ticks--;
			}
		}
	}

	//Dump_Interrupt_State(state);

	if(current->numTicks >= g_Quantum) {
		g_needReschedule[id] = true;
		// TODO:
		/*
		 * The current process is moved to a lower priority queue,
		 * since it consumed a full quantum.
		 */
	}

#ifdef RPI_BOOT

	/* To clear interrupt on RPI, write (1 << 1) to the control register */
	mmio_write(REG_TIMER_CR, (1 << 1));

	/* Write new value to load register */
	mmio_write(REG_TIMER_LOAD, mmio_read(REG_TIMER_CURRENT) + TIMER_CYCLES);

#else
	/* Disable timer module */
	mmio_unsetbit(REG_TIMER_CR, TIMER_ENABLE);

	/* Write new value to load register */
	mmio_write(REG_TIMER_LOAD, TIMER_COUNTER_INIT);

	/* Clear the interrupt from dual timer */
	mmio_write(REG_TIMER_INT_CLEAR, 0xffffffff);

	/* Enable timer module */
	mmio_setbit(REG_TIMER_CR, TIMER_ENABLE);
#endif

}

/* ----------------------------------------------------------------------
 * Public functions
 * ---------------------------------------------------------------------- */

void Init_Timer(void) {
	/*
	 * TODO: reprogram the timer to set the frequency.
	 * In bochs, it defaults to 18Hz, which is actually pretty
	 * reasonable.
	 */

	Print("Initializing timer...\n\r");

	/* Calibrate for delay loop */
	// Calibrate_Delay();
	// Print("Delay loop: %d iterations per tick\n", s_spinCountPerTick);

#ifdef RPI_BOOT
	/* Load initial counter */
	mmio_write(REG_TIMER_LOAD, mmio_read(REG_TIMER_CURRENT) + TIMER_CYCLES);
#else
	/* Program control register to one shot */
	mmio_setbit(REG_TIMER_CR, TIMER_ONE_SHOT);

	/* Load initial counter */
	mmio_write(REG_TIMER_LOAD, TIMER_COUNTER_INIT);

	/* Enable the timer module */
	mmio_setbit(REG_TIMER_CR, TIMER_ENABLE);
#endif

	/* Install the Timer IRQ on the PIC */
#ifdef RPI_BOOT
	Install_Shared_IRQ_Handler(TIMER_IRQ, &Timer_Interrupt_Handler);
#else
	Install_Primary_IRQ_Handler(TIMER_IRQ, &Timer_Interrupt_Handler);
#endif

	Print("Initializing Timer done\n\r");
}

int Start_Callback_Timer(int ticks, timerCallback cb) {
    int returned_timer_id;

    KASSERT(!Interrupts_Enabled());

    if(timeEventCount == MAX_TIMER_EVENTS) {
        Print
            ("timeEventCount == %d == MAX_TIMER_EVENTS; cannot start a new timer",
             MAX_TIMER_EVENTS);
        int i;
        for(i = 0; i < MAX_TIMER_EVENTS; i++) {
            Print("%d: cb 0x%p in %d/%d ticks", i,
                  pendingTimerEvents[i].callBack,
                  pendingTimerEvents[i].ticks,
                  pendingTimerEvents[i].origTicks);
        }
        return -1;
    } else {
        returned_timer_id = ++nextEventID;      /* avoid returning 0. */
        pendingTimerEvents[timeEventCount].id = returned_timer_id;
        pendingTimerEvents[timeEventCount].callBack = cb;
        pendingTimerEvents[timeEventCount].ticks = ticks;
        pendingTimerEvents[timeEventCount].origTicks = ticks;
        timeEventCount++;

        return returned_timer_id;
    }
}

int Get_Remaining_Callback_Timer_Ticks(int id) {
    int i;

    KASSERT(!Interrupts_Enabled());
    for(i = 0; i < timeEventCount; i++) {
        if(pendingTimerEvents[i].id == id) {
            return pendingTimerEvents[i].ticks;
        }
    }

    return -1;
}

int Cancel_Callback_Timer(int id) {
    int i;
    KASSERT(!Interrupts_Enabled());
    for(i = 0; i < timeEventCount; i++) {
        if(pendingTimerEvents[i].id == id) {
            if(timerDebug)
                Print
                    ("timer: event %d at %d ticks cancelled subscript %d/%d\n",
                     pendingTimerEvents[i].id,
                     pendingTimerEvents[i].ticks, i, timeEventCount);
            pendingTimerEvents[i] =
                pendingTimerEvents[timeEventCount - 1];
            timeEventCount--;
            return 0;
        }
    }

    Print("timer: unable to find timer id %d to cancel it\n", id);
    return -1;
}
