/*
 * GeekOS IDT initialization code
 * Copyright (c) 2001, David H. Hovemeyer <daveho@cs.umd.edu>
 * $Revision: 1.9 $
 * 
 * This is free software.  You are permitted to use,
 * redistribute, and modify it as specified in the file "COPYING".
 */

#include <geekos/kassert.h>
#include <geekos/defs.h>
#include <geekos/idt.h>
#include <geekos/smp.h>
#include <geekos/kthread.h>
#include <geekos/mmio.h>

#define IDT_DEBUG 0
#define Debug(args...) if(IDT_DEBUG) Print(args)

/* ARM has a Primary Interrupt Controller and Secondary Interrupt Controller */
/* This is a lookup table for functions to handle IRQ interrupts */
#ifdef RPI_BOOT
static Interrupt_Handler g_irqBasicHandlerTable[8];
#endif
static Interrupt_Handler g_irqPrimaryHandlerTable[32];
static Interrupt_Handler g_irqSecondaryHandlerTable[32];
static Interrupt_Handler g_svcHandler;
static Interrupt_Handler g_undHandler;
static Interrupt_Handler g_abtHandler;

/* ----------------------------------------------------------------------
 * Private data and functions
 * ---------------------------------------------------------------------- */

void Context_Switch_Handler(struct Interrupt_State * state) {

	Debug("Entering Context Switch Handler\n\r");

	/* Just check if need to schedule new thread, for now. */
	if(g_needReschedule[0]) {

		Debug("Scheduling new thread.\n\r");

		/* Make ourselves runnable again */
		Make_Runnable(CURRENT_THREAD);

		Schedule();

	} else {
		/* Don't need to keep kernel lock anymore if there's no rescheduling. */
		unlockKernel();
	}

	Debug("Exiting Context Switch Handler...\n\r");

}

void SVC_Handler(struct Interrupt_State * state) {

	Debug("SVC Handler entered\n\r");

	//Dump_Interrupt_State(state);

	lockKernel();
	KASSERT(!Interrupts_Enabled());

	g_svcHandler(state);

	Debug("Leaving SVC Handler...\n\r");

	Context_Switch_Handler(state);

}

void Undefined_Handler(struct Interrupt_State * state) {

	Debug("Undefined Handler entered\n\r");

	lockKernel();

	g_undHandler(state);

	Debug("Leaving Undefined Handler....BAD.\n\r");

	Context_Switch_Handler(state);

}

void Abort_Handler(struct Interrupt_State * state) {

	Debug("Entering Abort Handler...\n\r");

	lockKernel();

	g_abtHandler(state);

	Debug("Leaving Abort Handler...\n\r");

	Context_Switch_Handler(state);

}

void IRQ_Handler(struct Interrupt_State * state) {

	Debug("Entering IRQ Handler...\n\r");

	//Dump_Interrupt_State(state);

	lockKernel();

#ifdef RPI_BOOT

	/* There are three interrupt pending registers: the 'basic' (non-GPU shared) and two 'shared' interrupt registers. */

#if 0
	int i;
	for(i = 0; i < 8; i++) {
		if(mmio_readbit(REG_BASIC_INT_STATUS, i) && g_irqBasicHandlerTable[i]) {
			g_irqBasicHandlerTable[i](state);
		}
	}
#endif

	int i;
	for(i = 0; i < 32; i++) {
		if(mmio_readbit(REG_PIC_STATUS, i) && g_irqPrimaryHandlerTable[i]) {
			g_irqPrimaryHandlerTable[i](state);
		}
	}

	for(i = 0; i < 32; i++) {
		if(mmio_readbit(REG_SIC_STATUS, i) && g_irqSecondaryHandlerTable[i]) {
			g_irqSecondaryHandlerTable[i](state);
		}
	}

#else
	/* (QEMU only) There are just two interrupt pending registers: the primary controller and secondary controller. */
	int i;
	for(i = 0; i < 32; i++) {
		if(mmio_readbit(REG_PIC_STATUS, i) && g_irqPrimaryHandlerTable[i]) {
			g_irqPrimaryHandlerTable[i](state);
		}
	}

	if(mmio_readbit(REG_PIC_STATUS, 31)) {
		for(i = 0; i < 32; i++) {
			if( mmio_readbit(REG_SIC_STATUS, i) && g_irqSecondaryHandlerTable[i]) {
				g_irqSecondaryHandlerTable[i](state);
			}
		}
	}
#endif

	Debug("Leaving IRQ Handler...\n\r");

	Context_Switch_Handler(state);

}

/* ----------------------------------------------------------------------
 * Public functions
 * ---------------------------------------------------------------------- */

void Install_SVC_Handler(Interrupt_Handler handler) {
	g_svcHandler = handler;
}

void Install_Undefined_Handler(Interrupt_Handler handler) {
	g_undHandler = handler;
}

void Install_Abort_Handler(Interrupt_Handler handler) {
	g_abtHandler = handler;
}

#ifdef RPI_BOOT
void Install_Basic_IRQ_Handler(int irq, Interrupt_Handler handler) {
	g_irqBasicHandlerTable[irq] = handler;
	mmio_setbit(REG_BASIC_INT_ENABLE, irq);
}

void Install_Shared_IRQ_Handler(int irq, Interrupt_Handler handler) {

	if(irq <= 31) {
		g_irqPrimaryHandlerTable[irq] = handler;
		mmio_setbit(REG_PIC_ENABLE, irq);
	} else {
		g_irqSecondaryHandlerTable[irq - 32] = handler;
		mmio_setbit(REG_SIC_ENABLE, irq - 32);
	}

}

#else
void Install_Primary_IRQ_Handler(int irq, Interrupt_Handler handler) {
	
	/* Set the handler inside the table */
	g_irqPrimaryHandlerTable[irq] = handler;

	/* Enable the Primary IVC pin */
	mmio_setbit(REG_PIC_ENABLE, irq);
		
}

void Install_Secondary_IRQ_Handler(int irq, Interrupt_Handler handler) {

	g_irqSecondaryHandlerTable[irq] = handler;

	/* Enable the Secondary IVC pin */
	mmio_setbit(REG_SIC_ENABLE, irq);

	/* Set the SIC mask to allow passthrough to PIC */
	mmio_setbit(REG_SIC_PASSTHROUGH, irq);

}
#endif
