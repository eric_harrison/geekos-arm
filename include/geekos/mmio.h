/* Copyright (C) 2013 by John Cronin <jncronin@tysos.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef GEEKOS_MMIO_H
#define GEEKOS_MMIO_H

#include <geekos/ktypes.h>

void mmio_write(ulong_t reg, ulong_t data);
ulong_t mmio_read(ulong_t reg);

/* writebit writes a new value to the register with 1 bit set, setbit updates the register's current value */
void mmio_writebit(ulong_t reg, ulong_t bit);
void mmio_setbit(ulong_t reg, ulong_t bit);
void mmio_unsetbit(ulong_t reg, ulong_t bit);
unsigned char mmio_readbit(ulong_t reg, ulong_t bit);

#endif // GEEKOS_MMIO_H
