/*
 * Simple GPIO Interface to access pins.
 */

#ifndef GEEKOS_GPIO_H
#define GEEKOS_GPIO_H

#include <geekos/ktypes.h>

#define GPIO_RD 0
#define GPIO_WR 1

void gpio_sync();
void gpio_write(uint_t pin, unsigned char bit);
unsigned char gpio_read(uint_t pin);
void gpio_function_select(uint_t pin, unsigned char function);

#endif
