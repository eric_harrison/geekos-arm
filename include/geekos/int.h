/*
 * GeekOS interrupt handling data structures and functions
 * Copyright (c) 2001, David H. Hovemeyer <daveho@cs.umd.edu>
 *
 * All rights reserved.
 *
 * This code may not be resdistributed without the permission of the copyright holders.
 * Any student solutions using any of this code base constitute derviced work and may
 * not be redistributed in any form.  This includes (but is not limited to) posting on
 * public forums or web sites, providing copies to (past, present, or future) students
 * enrolled in similar operating systems courses the University of Maryland's CMSC412 course.
 *
 * $Revision: 1.12 $
 * 
 */

/*
 * This module describes the C interface which must be implemented
 * by interrupt handlers, and has the initialization function
 * for the interrupt system as a whole.
 */

#ifndef GEEKOS_INT_H
#define GEEKOS_INT_H

#include <geekos/kassert.h>
#include <geekos/ktypes.h>
#include <geekos/defs.h>
#include <geekos/lock.h>

/*
 * This struct reflects the contents of the stack when
 * a C interrupt handler function is called.
 * It must be kept up to date with the code in "lowlevel.asm".
 */
struct Interrupt_State {
    /*
     * The register contents at the time of the exception.
     * We save these explicitly.
     */
	uint_t spsr;
	uint_t r0;
	uint_t r1;
	uint_t r2;
	uint_t r3;
	uint_t r4;
	uint_t r5;
	uint_t r6;
	uint_t r7;
	uint_t r8;
	uint_t r9;
	uint_t r10;
	uint_t r11;
	uint_t r12;
	uint_t sp;
	uint_t lr;
	uint_t pc;
};

/*
 * An interrupt that occurred in user mode.
 * If Is_User_Interrupt(state) returns true, then the
 * Interrupt_State object may be cast to this kind of struct.
 */
struct User_Interrupt_State {
    struct Interrupt_State state;
};

static __inline__ bool Is_User_Interrupt(struct Interrupt_State *state) {
	return true;
}

/*
 * The interrupt flag bit in the eflags register.
 * FIXME: should be in something like "cpu.h".
 */
#define EFLAGS_IF (1 << 9)

/*
 * The signature of an interrupt handler.
 */
typedef void (*Interrupt_Handler) (struct Interrupt_State * state);

/*
 * Perform all low- and high-level initialization of the
 * interrupt system.
 */
void Init_Interrupts(int secondaryCPU);

/*
 * Query whether or not interrupts are currently enabled.
 */
bool Interrupts_Enabled(void);

Spin_Lock_t intLock;

extern void lockKernel();
extern void unlockKernel();
extern bool Kernel_Is_Locked();

/*
 * Block interrupts.
 */
static __inline__ void __Disable_Interrupts(void) {
	unsigned int val;
	/* Interrupts are handled in CPSR on ARM */
	__asm__ __volatile__("mrs %[result], cpsr_all" : [result]"=r" (val));
	__asm__ __volatile__("orr %[result], %[value], #1<<7" : [result]"=r"(val) : [value]"r" (val));
	__asm__ __volatile__("msr cpsr_all, %[value]" : : [value]"r" (val)); 
	lockKernel();
}

#define Disable_Interrupts()		\
do {					\
    KASSERT(Interrupts_Enabled());	\
    __Disable_Interrupts();		\
} while (0)

/*
 * Unblock interrupts.
 */
static __inline__ void __Enable_Interrupts(void) {
    unlockKernel();
		unsigned int val;
		/* Interrupts are handled in CPSR on ARM */
		__asm__ __volatile__("mrs %[result], cpsr_all" : [result]"=r" (val));
		__asm__ __volatile__("bic %[result], %[value], #1<<7" : [result]"=r"(val) : [value]"r" (val));
		__asm__ __volatile__("msr cpsr_all, %[value]" : : [value]"r" (val)); 
}

#define Enable_Interrupts()		\
do {					\
    KASSERT(!Interrupts_Enabled());	\
    KASSERT(Kernel_Is_Locked());	\
    __Enable_Interrupts();		\
} while (0)

/*
 * Dump interrupt state struct to screen
 */
void Dump_Interrupt_State(struct Interrupt_State *state);

/**
 * Start interrupt-atomic region.
 * @return true if interrupts were enabled at beginning of call,
 * false otherwise.
 */
static __inline__ bool Begin_Int_Atomic(void) {
    bool enabled = Interrupts_Enabled();
    if(enabled)
        Disable_Interrupts();
    else {
        /* ns 2014; this may be a bad idea, but it brings consistency to the migration
           of interrupt disabling === holding the great big lock in cases where only the
           interrupts are already disabled but the lock is not held. */
        if(!Kernel_Is_Locked()) {
            lockKernel();
        }
    }
    return enabled;
}

/**
 * End interrupt-atomic region.
 * @param iflag the value returned from the original Begin_Int_Atomic() call.
 */
static __inline__ void End_Int_Atomic(bool iflag) {
    KASSERT(!Interrupts_Enabled());
    if(iflag) {
        /* Interrupts were originally enabled, so turn them back on */
        Enable_Interrupts();
    }
}

#endif /* GEEKOS_INT_H */
