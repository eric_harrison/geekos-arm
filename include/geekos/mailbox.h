/*
 * GeekOS Mailbox Interface
 * Copyright (c) 2001, David H. Hovemeyer <daveho@cs.umd.edu>
 *
 * All rights reserved.
 *
 * This code may not be resdistributed without the permission of the copyright holders.
 * Any student solutions using any of this code base constitute derviced work and may
 * not be redistributed in any form.  This includes (but is not limited to) posting on
 * public forums or web sites, providing copies to (past, present, or future) students
 * enrolled in similar operating systems courses the University of Maryland's CMSC412 course.
 *
 * $Revision: 1.12 $
 * 
 */

/*
 * This module provides an interface for accessing the Raspberry Pi Mailbox.
 */

#ifndef GEEKOS_MAILBOX_H
#define GEEKOS_MAILBOX_H

unsigned int readmailbox(unsigned int channel);
void writemailbox(unsigned int channel, unsigned int data);

#endif
