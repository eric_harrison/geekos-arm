/*
 * GeekOS Screen Interface
 * Copyright (c) 2001, David H. Hovemeyer <daveho@cs.umd.edu>
 *
 * All rights reserved.
 *
 * This code may not be resdistributed without the permission of the copyright holders.
 * Any student solutions using any of this code base constitute derviced work and may
 * not be redistributed in any form.  This includes (but is not limited to) posting on
 * public forums or web sites, providing copies to (past, present, or future) students
 * enrolled in similar operating systems courses the University of Maryland's CMSC412 course.
 *
 * $Revision: 1.12 $
 * 
 */

/*
 * Interface for printing to/from Raspberry Pi
 */

#include <geekos/ktypes.h>

#ifndef GEEKOS_SCREEN_H
#define GEEKOS_SCREEN_H

#define BLACK   0
#define BLUE    1
#define GREEN   2
#define CYAN    3
#define RED     4
#define MAGENTA 5
#define AMBER   6
#define GRAY    7
#define BRIGHT  8
#define ATTRIB(bg,fg) ((fg)|((bg)<<4))

#define NUMCOLS 80
#define NUMROWS 25

#define TABWIDTH 8

void Print(const char *fmt, ...) __attribute__ ((format(printf, 1, 2)));
void PrintString(const unsigned char * buf, ulong_t length);

#endif
