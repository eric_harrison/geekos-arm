/*
 * GeekOS IDT initialization code
 * Copyright (c) 2001, David H. Hovemeyer <daveho@cs.umd.edu>
 *
 * All rights reserved.
 *
 * This code may not be resdistributed without the permission of the copyright holders.
 * Any student solutions using any of this code base constitute derviced work and may
 * not be redistributed in any form.  This includes (but is not limited to) posting on
 * public forums or web sites, providing copies to (past, present, or future) students
 * enrolled in similar operating systems courses the University of Maryland's CMSC412 course.
 *
 * $Revision: 1.10 $
 * 
 */

#ifndef GEEKOS_IDT_H
#define GEEKOS_IDT_H

#include <geekos/int.h>

#ifdef RPI_BOOT

#define TIMER_IRQ 1
#define UART0_IRQ 57

#else

#define TIMER_IRQ 4
#define UART0_IRQ 12

#endif

#define IDT_START 0
#define IDT_SIZE (4*7)

void Install_SVC_Handler(Interrupt_Handler handler);
void Install_Undefined_Handler(Interrupt_Handler handler);
void Install_Abort_Handler(Interrupt_Handler handler);

#ifdef RPI_BOOT
void Install_Basic_IRQ_Handler(int irq, Interrupt_Handler handler);
void Install_Shared_IRQ_Handler(int irq, Interrupt_Handler handler);
#else
void Install_Primary_IRQ_Handler(int irq, Interrupt_Handler handler);
void Install_Secondary_IRQ_Handler(int irq, Interrupt_Handler handler);
#endif

#endif /* GEEKOS_IDT_H */
