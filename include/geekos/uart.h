/*
 * GeekOS UART Interface
 * Copyright (c) 2001, David H. Hovemeyer <daveho@cs.umd.edu>
 *
 * All rights reserved.
 *
 * This code may not be resdistributed without the permission of the copyright holders.
 * Any student solutions using any of this code base constitute derviced work and may
 * not be redistributed in any form.  This includes (but is not limited to) posting on
 * public forums or web sites, providing copies to (past, present, or future) students
 * enrolled in similar operating systems courses the University of Maryland's CMSC412 course.
 *
 * $Revision: 1.12 $
 * 
 */

/*
 * This module provides an interface for accessing the Universal Asynchronous Receiver Transmitter
 * (UART), which can read from/write to a serial port for the Raspberry Pi.
 */


#ifndef GEEKOS_UART_H
#define GEEKOS_UART_H

#include <geekos/ktypes.h>

void Init_UART();

unsigned char UART_Get_Char();
void UART_Write(const unsigned char* buffer, ulong_t size);
void UART_PrintString(const char* str);
void UART_Print_Char(int ch);

#endif
