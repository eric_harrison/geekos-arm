/*
 * GeekOS timer interrupt support
 * Copyright (c) 2001, David H. Hovemeyer <daveho@cs.umd.edu>
 *
 * All rights reserved.
 *
 * This code may not be resdistributed without the permission of the copyright holders.
 * Any student solutions using any of this code base constitute derviced work and may
 * not be redistributed in any form.  This includes (but is not limited to) posting on
 * public forums or web sites, providing copies to (past, present, or future) students
 * enrolled in similar operating systems courses the University of Maryland's CMSC412 course.
 *
 * $Revision: 1.15 $
 * 
 */

#ifndef GEEKOS_TIMER_H
#define GEEKOS_TIMER_H

#include <geekos/defs.h>
#include <geekos/ktypes.h>

#ifdef RPI_BOOT

/* One cycle every 1 MHZ on RPI */
#define TIMER_CYCLES 1000000

#else

#define TIMER_COUNTER_INIT 0xfffff
#define TIMER_ONE_SHOT 0
#define TIMER_ENABLE 7

#endif

#define MAX_TIMER_EVENTS 100
// TODO: not entirely clear what this number should be. I know on RPI the timer clock speed is 1 MHz.
#define TICKS_PER_SEC 1000
#define MS_PER_TICK (1000.0f / (float)TICKS_PER_SEC)
#define TICKS_PER_MS ((float)TICKS_PER_SEC / 1000.0f)
#define TICKS_PER_US ((float)TICKS_PER_SEC / 1000000.0f)

extern volatile ulong_t g_numTicks;

void Micro_Delay(int us);

void Init_Timer(void);
void Init_Timer_Interrupt(void);

typedef void (*timerCallback) (int);

typedef struct {
	ulong_t endTime;
	int rollover;
} timerEvent;

typedef struct {
	int ticks;                  /* timer code decrements this */
	int id;                     /* unqiue id for this timer even */
	timerCallback callBack;     /* Queue to wakeup on timer expire */
	int origTicks;
} callbackTimerEvent;

int Start_Callback_Timer(int ticks, timerCallback);
int Get_Remaing_Callback_Timer_Ticks(int id);
int Cancel_Callback_Timer(int id);

timerEvent Start_Timer(ulong_t usec);
int Check_Timer(timerEvent event);


#endif /* GEEKOS_TIMER_H */
