/*
 * Misc. kernel definitions
 * Copyright (c) 2001,2003,2004 David H. Hovemeyer <daveho@cs.umd.edu>
 * Copyright (c) 2003,2013,2014 Jeffrey K. Hollingsworth <hollings@cs.umd.edu>
 *
 * All rights reserved.
 *
 * This code may not be resdistributed without the permission of the copyright holders.
 * Any student solutions using any of this code base constitute derviced work and may
 * not be redistributed in any form.  This includes (but is not limited to) posting on
 * public forums or web sites, providing copies to (past, present, or future) students
 * enrolled in similar operating systems courses the University of Maryland's CMSC412 course.
 *
 * $Revision: 1.11 $
 * 
 */

#ifndef GEEKOS_DEFS_H
#define GEEKOS_DEFS_H

/*
 * Address where kernel is loaded
 */
#define KERNEL_START_ADDR 0x8000

/*
 * Pages for initial kernel thread context object, and stack.
 * Keep these up to date with defs.asm.
 */
#define KERNEL_IRQ_STACK (KERNEL_START_ADDR - 4096)
#define KERNEL_STACK (KERNEL_IRQ_STACK - 4096)
#define KERNEL_THREAD_OBJ (KERNEL_STACK - 4096)

/*
 * Kernel and user privilege levels
 */
#define KERNEL_PRIVILEGE 0
#define USER_PRIVILEGE 3

/*
 * Memory mapped registers.
 */

#ifdef RPI_BOOT

/* 
 * All registers taken from RPI Datasheet available at
 * https://www.raspberrypi.org/wp-content/uploads/2012/02/BCM2835-ARM-Peripherals.pdf
 */

#define REG_GPIO 0x20200000
#define REG_UART0 0x20201000
#define REG_IC 0x2000B000
#define REG_TIMER 0x20003000

#define REG_BASIC_INT_STATUS (REG_IC + 0x200)
#define REG_PIC_STATUS (REG_IC + 0x204)
#define REG_SIC_STATUS (REG_IC + 0x208)

#define REG_BASIC_INT_ENABLE (REG_IC + 0x218)
#define REG_PIC_ENABLE (REG_IC + 0x210)
#define REG_SIC_ENABLE (REG_IC + 0x214)

#define REG_TIMER_CR (REG_TIMER + 0x00)
#define REG_TIMER_CURRENT (REG_TIMER + 0x04)
#define REG_TIMER_LOAD (REG_TIMER + 0x10)

#define NUM_GPIO_PINS 54

#define REG_GPIO_SEL(x) (REG_GPIO + 0x04*((x)/10))

#define REG_GPIO_SET0 (REG_GPIO + 0x1C)
#define REG_GPIO_SET1 (REG_GPIO + 0x20)

#define REG_GPIO_CLEAR0 (REG_GPIO + 0x28)
#define REG_GPIO_CLEAR1 (REG_GPIO + 0x2C)

#define REG_GPIO_LEV0 (REG_GPIO + 0x34)
#define REG_GPIO_LEV1 (REG_GPIO + 0x38)

#else

#define REG_GPIO 0x101e4000
#define REG_UART0 0x101f1000
#define REG_PIC 0x10140000
#define REG_SIC 0x10003000
#define REG_TIMER 0x101e2000

#define REG_PIC_STATUS (REG_PIC + 0x00)
#define REG_PIC_ENABLE (REG_PIC + 0x10)

#define REG_SIC_STATUS (REG_SIC + 0x00)
#define REG_SIC_ENABLE (REG_SIC + 0x08)
#define REG_SIC_PASSTHROUGH (REG_SIC + 0x20)

#define PIC_SIC_PIN 1

#define REG_TIMER_CURRENT (REG_TIMER + 0x04)
#define REG_TIMER_CR (REG_TIMER + 0x08)
#define REG_TIMER_LOAD (REG_TIMER + 0x00)
#define REG_TIMER_INT_CLEAR (REG_TIMER + 0x0c)

#endif

#define CPSR_INTDISABLE 7

#define REG_UART_RAWINT (REG_UART0 + 0x3c)
#define REG_UART_INT_CLEAR (REG_UART0 + 0x44)

#define UART_RXIC 4

/*
 * x86 (and ARM, if you set it correctly) has 4096 byte pages
 */
#define PAGE_POWER 12
#define PAGE_SIZE (1<<PAGE_POWER)
#define PAGE_MASK (~(0xffffffff << PAGE_POWER))

#endif /* GEEKOS_DEFS_H */
